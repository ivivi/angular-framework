USE [App]
GO

DELETE FROM dbo.Sys_Log;
DELETE FROM dbo.Sys_Menu;
DELETE FROM dbo.Sys_Role;
DELETE FROM dbo.Sys_Module;
DELETE FROM dbo.Sys_User;
DELETE FROM dbo.Vendor_ConsultantAttachfile;
DELETE FROM dbo.Vendor_Consultant;
DELETE FROM dbo.Vendor_VendorAttachfile;
DELETE FROM dbo.Vendor_Vendor;

SET IDENTITY_INSERT [dbo].[Sys_Role] ON  
INSERT [dbo].[Sys_Role] ([Id], [Name], [ModulePermissions]) VALUES (4, N'User', N'Home=View')
INSERT [dbo].[Sys_Role] ([Id], [Name], [ModulePermissions]) VALUES (5, N'Administrator', N'Vendor=View;Home=View;System=ALL,Menu,MenuEdit,Role,RoleEdit,User,UserEdit,Module,ModuleEdit;Form=View')
SET IDENTITY_INSERT [dbo].[Sys_Role] OFF

SET IDENTITY_INSERT [dbo].[Sys_User] ON 

INSERT [dbo].[Sys_User] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (4, N'ivivi@outlook.com', 0, NULL, N'bd7ddc19-a132-475a-a209-ec2570f9dd9c', NULL, 0, 0, NULL, 0, 0, N'ivivi')
INSERT [dbo].[Sys_User] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (5, N'zhangsan@baidu.com', 0, N'', N'', N'23222rrr', 0, 0, NULL, 0, 0, N'eee')
SET IDENTITY_INSERT [dbo].[Sys_User] OFF
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId], [RoleEntity_Id], [UserEntity_Id]) VALUES (4, 4, NULL, NULL)
INSERT [dbo].[Sys_UserRole] ([UserId], [RoleId], [RoleEntity_Id], [UserEntity_Id]) VALUES (5, 4, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Vendor_Vendor] ON 

INSERT [dbo].[Vendor_Vendor] ([Id], [CompanyName], [CompanyAddress], [ContactPersion], [Tel], [PostCode], [Email], [Combination], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [Remark]) VALUES (9, N'百度北京有限公司', N'北京市海淀区上地10街xxx', N'李XXX', N'187000000001', N'010', N'baidu@baidu.com', N'baidu', CAST(N'2016-06-06 17:26:45.763' AS DateTime), N'ivivi', CAST(N'2016-06-06 19:37:17.177' AS DateTime), N'ivivi', N'百度-备注')
INSERT [dbo].[Vendor_Vendor] ([Id], [CompanyName], [CompanyAddress], [ContactPersion], [Tel], [PostCode], [Email], [Combination], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [Remark]) VALUES (10, N'新浪科技有限公司', N'北京市海淀区XX街XX号', N'XXX', N'187000000001', N'010', N'sina@sina.com.cn', N'sina', CAST(N'2016-06-06 18:58:08.447' AS DateTime), N'ivivi', CAST(N'2016-06-06 18:58:08.447' AS DateTime), N'ivivi', N'sss')
SET IDENTITY_INSERT [dbo].[Vendor_Vendor] OFF
SET IDENTITY_INSERT [dbo].[Vendor_Consultant] ON 

INSERT [dbo].[Vendor_Consultant] ([Id], [Name], [Address], [Tel], [Email], [VendorId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [Remark]) VALUES (1, N'张三', N'上海市XX路XX街道0号', N'18500870237', N'zhangsan@baidu.com', 9, CAST(N'2016-06-07 16:07:22.307' AS DateTime), N'ivivi', CAST(N'2016-06-07 16:07:22.307' AS DateTime), N'ivivi', NULL)
INSERT [dbo].[Vendor_Consultant] ([Id], [Name], [Address], [Tel], [Email], [VendorId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [Remark]) VALUES (2, N'张三', N'上海市XX路XX街道0号', N'18500870237', N'liyf@samsundot.com', 10, CAST(N'2016-06-07 16:59:11.387' AS DateTime), N'ivivi', CAST(N'2016-06-07 16:59:11.387' AS DateTime), N'ivivi', N'')
INSERT [dbo].[Vendor_Consultant] ([Id], [Name], [Address], [Tel], [Email], [VendorId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [Remark]) VALUES (3, N'css', N'sf', N'sfsf', N'sfs', 9, CAST(N'2016-08-08 14:08:31.950' AS DateTime), N'ivivi', CAST(N'2016-08-08 14:08:31.950' AS DateTime), N'ivivi', N'fsfs')
SET IDENTITY_INSERT [dbo].[Vendor_Consultant] OFF
SET IDENTITY_INSERT [dbo].[Vendor_ConsultantAttachfile] ON 

INSERT [dbo].[Vendor_ConsultantAttachfile] ([Id], [ConsultantId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, NULL, N'zhupeippei.jpg', N'E:\UploadFile\2016\6\\b181f35f-f49e-4b8c-9492-b9995945533c.jpg', N'33', CAST(N'2016-06-07 15:37:13.287' AS DateTime), N'ivivi', CAST(N'2016-06-07 15:37:13.287' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_ConsultantAttachfile] ([Id], [ConsultantId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, NULL, N'zhupeippei.jpg', N'E:\UploadFile\2016\6\\f2886e97-976e-4261-831b-4d170ba49900.jpg', N'33', CAST(N'2016-06-07 15:40:59.607' AS DateTime), N'ivivi', CAST(N'2016-06-07 15:40:59.607' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_ConsultantAttachfile] ([Id], [ConsultantId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, NULL, N'zhupeippei.jpg', N'E:\UploadFile\2016\6\\1d360ed4-3d23-4fc4-833b-f0f1a35b4c50.jpg', N'33', CAST(N'2016-06-07 15:41:06.997' AS DateTime), N'ivivi', CAST(N'2016-06-07 15:41:06.997' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_ConsultantAttachfile] ([Id], [ConsultantId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, NULL, N'zhupeippei.jpg', N'E:\UploadFile\2016\6\\fae4bba3-b796-4a56-a629-2b430d35b35e.jpg', N'33', CAST(N'2016-06-07 16:01:55.743' AS DateTime), N'ivivi', CAST(N'2016-06-07 16:01:55.743' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_ConsultantAttachfile] ([Id], [ConsultantId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, 1, N'zhupeippei.jpg', N'E:\UploadFile\2016\6\\fd6ed08c-6214-4d5b-a5b9-37ffa9dcc63c.jpg', N'33', CAST(N'2016-06-07 16:07:20.890' AS DateTime), N'ivivi', CAST(N'2016-06-07 16:07:22.307' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_ConsultantAttachfile] ([Id], [ConsultantId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (6, 2, N'zhupeippei.jpg', N'E:\UploadFile\2016\6\\e2b63b0e-f096-408a-afbf-560434d62abf.jpg', N'33', CAST(N'2016-06-07 16:51:17.637' AS DateTime), N'ivivi', CAST(N'2016-06-07 16:59:11.387' AS DateTime), N'ivivi')
SET IDENTITY_INSERT [dbo].[Vendor_ConsultantAttachfile] OFF
SET IDENTITY_INSERT [dbo].[Vendor_VendorAttachfile] ON 

INSERT [dbo].[Vendor_VendorAttachfile] ([Id], [VendorId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (39, 9, N'2 (1).jpg', N'E:\UploadFile\2016\6\\0a9ee29d-2e32-4809-9f59-00132924f15c.jpg', N'214', CAST(N'2016-06-06 17:26:44.887' AS DateTime), N'ivivi', CAST(N'2016-06-06 17:26:45.763' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_VendorAttachfile] ([Id], [VendorId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (40, NULL, N'2.jpg', N'E:\UploadFile\2016\6\\45fcfb80-0875-45a5-99df-07ec1aae3d57.jpg', N'214', CAST(N'2016-06-06 19:28:19.987' AS DateTime), N'ivivi', CAST(N'2016-06-06 19:28:19.987' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_VendorAttachfile] ([Id], [VendorId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (41, NULL, N'2.jpg', N'E:\UploadFile\2016\6\\d6133d1e-c0e4-46ba-8352-dc6eac57ae94.jpg', N'214', CAST(N'2016-06-06 19:28:54.530' AS DateTime), N'ivivi', CAST(N'2016-06-06 19:28:54.530' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_VendorAttachfile] ([Id], [VendorId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (42, NULL, N'2.jpg', N'E:\UploadFile\2016\6\\cd1a4a6e-1ebc-401b-aa17-b37517707132.jpg', N'214', CAST(N'2016-06-06 19:32:15.317' AS DateTime), N'ivivi', CAST(N'2016-06-06 19:32:15.317' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_VendorAttachfile] ([Id], [VendorId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (43, 9, N'2.jpg', N'E:\UploadFile\2016\6\\d2d6f0a1-c547-4cc7-ac5e-894a1f875c57.jpg', N'214', CAST(N'2016-06-06 19:34:46.750' AS DateTime), N'ivivi', CAST(N'2016-06-06 19:37:17.150' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_VendorAttachfile] ([Id], [VendorId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (44, NULL, N'Requirements List.xlsx', N'E:\UploadFile\2016\8\\8a0881c6-461b-4353-9413-22ba3a5ecc06.xlsx', N'27', CAST(N'2016-08-10 17:20:48.987' AS DateTime), N'ivivi', CAST(N'2016-08-10 17:20:48.987' AS DateTime), N'ivivi')
INSERT [dbo].[Vendor_VendorAttachfile] ([Id], [VendorId], [FileName], [FileSavedPath], [FileSize], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (45, NULL, N'Requirements List.xlsx', N'E:\UploadFile\2016\8\\c91abf30-e186-40b9-b7df-f5f3cb0f4746.xlsx', N'27', CAST(N'2016-08-11 10:08:14.277' AS DateTime), N'ivivi', CAST(N'2016-08-11 10:08:14.277' AS DateTime), N'ivivi')
SET IDENTITY_INSERT [dbo].[Vendor_VendorAttachfile] OFF
SET IDENTITY_INSERT [dbo].[Sys_Menu] ON 

INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (19, N'首页', N'home', 0, CAST(N'2016-05-27 16:59:11.837' AS DateTime), N'', CAST(N'2016-08-10 13:43:07.813' AS DateTime), N'ivivi', N'/#/', NULL, 0)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (20, N'供应商管理', N'business', 0, CAST(N'2016-05-27 16:59:12.163' AS DateTime), N'', CAST(N'2016-10-25 16:07:58.483' AS DateTime), N'ivivi', N'', N'Vendor=View', 4)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (21, N'供应商信息', N'business', 20, CAST(N'2016-05-27 16:59:12.170' AS DateTime), N'', CAST(N'2016-10-25 16:08:10.330' AS DateTime), N'ivivi', N'/#/Vendor/Vendor', N'Vendor=View', 0)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (22, N'顾问信息', N'contacts', 20, CAST(N'2016-05-27 16:59:12.177' AS DateTime), N'', CAST(N'2016-10-25 16:08:06.617' AS DateTime), N'ivivi', N'/#/Vendor/Consultant', N'Vendor=View', 0)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (23, N'系统管理', N'settings', 0, CAST(N'2016-08-08 11:49:02.963' AS DateTime), N'', CAST(N'2016-10-25 16:38:14.840' AS DateTime), N'ivivi', N'', N'System=All', 20)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (24, N'菜单管理', N'list', 23, CAST(N'2016-08-08 11:49:03.420' AS DateTime), N'', CAST(N'2016-10-25 16:38:36.180' AS DateTime), N'ivivi', N'/#/System/Menu', N'System=All', 0)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (25, N'角色管理', N'group', 23, CAST(N'2016-08-08 11:49:03.450' AS DateTime), N'', CAST(N'2016-10-25 16:38:28.127' AS DateTime), N'ivivi', N'/#/System/Role', N'System=All', 0)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (28, N'首页', N'fa fa-home', 233, CAST(N'2016-08-09 17:46:26.397' AS DateTime), N'ivivi', CAST(N'2016-08-09 17:46:26.397' AS DateTime), N'ivivi', N'/#/', NULL, 23)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (33, N'用户管理', N'person', 23, CAST(N'2016-08-09 18:51:58.077' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:38:40.527' AS DateTime), N'ivivi', N'/#/System/User', N'System=All', 3)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (34, N'模块管理', N'view_module', 23, CAST(N'2016-08-10 16:52:04.397' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:38:44.087' AS DateTime), N'ivivi', N'/#/System/Module', N'System=All', 5)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (35, N'表单', N'table', 0, CAST(N'2016-09-01 10:11:10.670' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:39:19.737' AS DateTime), N'ivivi', N'', N'Form=View', 6)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (36, N'图标', N'image', 35, CAST(N'2016-09-01 10:17:00.573' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:39:28.587' AS DateTime), N'ivivi', N'/#/Form/Icons', N'Form=View', 0)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (37, N'按钮', N'all_out', 35, CAST(N'2016-09-01 10:23:05.850' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:39:33.447' AS DateTime), N'ivivi', N'/#/Form/Buttons', N'Form=View', 4)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (38, N'文本', N'lists', 35, CAST(N'2016-09-06 10:16:13.293' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:39:37.163' AS DateTime), N'ivivi', N'/#/Form/Texts', N'Form=View', 6)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (39, N'表格', N'format_list_numbered_black', 35, CAST(N'2016-09-06 10:24:59.817' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:39:41.150' AS DateTime), N'ivivi', N'/#/Form/Tables', N'Form=View', 8)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (40, N'输入按钮', N'border_color', 35, CAST(N'2016-09-06 10:32:39.997' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:39:48.283' AS DateTime), N'ivivi', N'/#/Form/Inputs', N'Form=View', 10)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (41, N'单选按钮', N'check_circle_black', 35, CAST(N'2016-09-06 10:36:55.460' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:39:44.777' AS DateTime), N'ivivi', N'/#/Form/Radios', N'Form=View', 12)
INSERT [dbo].[Sys_Menu] ([Id], [Title], [Icon], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [URL], [View], [SortNo]) VALUES (43, N'多选', N'check_box_black', 35, CAST(N'2016-09-06 11:01:13.470' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:39:51.583' AS DateTime), N'ivivi', N'/#/Form/Checkboxs', N'Form=View', 14)
SET IDENTITY_INSERT [dbo].[Sys_Menu] OFF
SET IDENTITY_INSERT [dbo].[Sys_Module] ON 

INSERT [dbo].[Sys_Module] ([Id], [Module], [Permissions], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, N'Vendor', N'View', CAST(N'2016-08-10 17:16:21.817' AS DateTime), N'ivivi', CAST(N'2016-08-10 17:16:21.817' AS DateTime), N'ivivi')
INSERT [dbo].[Sys_Module] ([Id], [Module], [Permissions], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, N'Home', N'View', CAST(N'2016-08-31 21:27:59.993' AS DateTime), N'ivivi', CAST(N'2016-08-31 21:27:59.993' AS DateTime), N'ivivi')
INSERT [dbo].[Sys_Module] ([Id], [Module], [Permissions], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, N'System', N'ALL,Menu,MenuEdit,Role,RoleEdit,User,UserEdit,Module,ModuleEdit', CAST(N'2016-08-31 21:29:32.313' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:37:49.700' AS DateTime), N'ivivi')
INSERT [dbo].[Sys_Module] ([Id], [Module], [Permissions], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, N'Form', N'View', CAST(N'2016-09-01 09:31:10.633' AS DateTime), N'ivivi', CAST(N'2016-10-25 16:39:03.917' AS DateTime), N'ivivi')
SET IDENTITY_INSERT [dbo].[Sys_Module] OFF

﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using Owin;
using Microsoft.Owin;
using App.Web.Capsule;
using App.Web.Mapping; 

[assembly: OwinStartupAttribute(typeof(App.Web.Startup))]
namespace App.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //Auth config
            ConfigureAuth(app);

            //Mapping config
            var mappingDefinitions = new MappingDefinitions();
            mappingDefinitions.Initialise();

            //http config
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();

            //Ioc config
            new WebCapsule().Initialise(config);

            //Mvc config
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AreaRegistration.RegisterAllAreas();  

            //Web api config
            WebApiConfig.Register(config); 
            app.UseWebApi(config);  
        }
    }
}

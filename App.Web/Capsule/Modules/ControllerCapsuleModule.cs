﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Autofac.Integration.WebApi;
using App.Web.References;
using Autofac.Integration.Mvc;

namespace App.Web.Capsule.Modules
{
    public class ControllerCapsuleModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(ReferencedAssemblies.Web);
            builder.RegisterApiControllers(ReferencedAssemblies.Web);
        }
    }
}
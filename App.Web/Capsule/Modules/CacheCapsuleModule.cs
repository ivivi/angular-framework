﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Autofac.Core;
using App.Core.Service;
using App.Service.Service;
using App.Web.References;
namespace App.Web.Capsule.Modules
{
    public class CacheCapsuleModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ReferencedAssemblies.Cache).
                Where(_ => _.Name.EndsWith("CacheManager")).
                AsImplementedInterfaces().InstancePerLifetimeScope(); 
        }
    }
}
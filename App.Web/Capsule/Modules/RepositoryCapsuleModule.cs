﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using App.Core.Entities;
using App.Core.Data.Repositories;
using App.Data.Repositories;
using App.Web.References;

namespace App.Web.Capsule.Modules
{
    public class RepositoryCapsuleModule:Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ReferencedAssemblies.Repositories).
               Where(_ => _.Name.EndsWith("Repository")).
               AsImplementedInterfaces().
               InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(BaseRepository<>)).As(typeof(IRepository<>)).InstancePerDependency();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using App.Core.Entities;
using App.Core.Entities.Identity;
using App.Core.Service.Identity;
using App.Core.Cache;
using App.Web.Filter;

namespace App.Web.Controllers
{  
    public class HomeController : Controller
    {
        readonly IUserCacheManager iUserCacheManager;
        readonly IUserService iUserService;
        readonly ICacheManager iCacheManager;
        public HomeController(ICacheManager cacheManager,
            IUserService userService,
            IUserCacheManager userCacheManager)
        {
            this.iUserCacheManager = userCacheManager;
            this.iUserService = userService;
            this.iCacheManager = cacheManager;
        } 
        /// <summary>
        /// 首页
        /// </summary> 
        public ActionResult Index()
        {
            this.iUserCacheManager.Reset();
            return Redirect("/App/index.html");
        } 
    }
}
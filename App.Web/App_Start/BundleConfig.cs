﻿using System.Web;
using System.Web.Optimization;

namespace App.Web
{
    public class BundleConfig
    {
        // 有关绑定的详细信息，请访问 http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Assets/Scripts/JS").Include(
                "~/Assets/Scripts/jquery-{version}.js", 
                "~/Assets/Scripts/respond.js", 
                "~/Assets/Scripts/material.js", 
                "~/Assets/Scripts/angular/angular.js",
                "~/Assets/Scripts/jcs-auto-validate.js",
                "~/Assets/Scripts/angular-pagination.js",
                "~/Assets/Scripts/FileAPI.js",
                "~/Assets/Scripts/ng-file-upload-all.js",
                "~/Assets/Scripts/angular-block-ui.js",
                "~/Assets/Scripts/ngDialog.js",
                "~/Assets/Scripts/angular-select2.js",
                "~/Assets/Scripts/angular/angular-route.js",
                "~/Assets/Scripts/angular/angular-resource.js",
                "~/Assets/Scripts/angular/angular-animate.js",
                "~/Assets/Scripts/angular/angular-loader.js",
                "~/Assets/Scripts/angular/angular-aria.js",
                "~/Assets/Scripts/angular/angular-cookies.js",
                "~/Assets/Scripts/angular/angular-message-format.js",
                "~/Assets/Scripts/angular/angular-messages.js",
                "~/Assets/Scripts/angular/angular-mocks.js",
                "~/Assets/Scripts/angular/angular-sanitize.js",
                "~/Assets/Scripts/angular/angular-scenario.js",
                "~/Assets/Scripts/angular/angular-touch.js",
                "~/Assets/Scripts/angular/i18n/angular-locale_en-us.js",
                
                "~/Assets/Scripts/angular/ui-bootstrap.js",
                "~/Assets/Scripts/angular/ui-bootstrap-tpls.js",
                "~/App/app.js"
                ));

            bundles.Add(new StyleBundle("~/Assets/Styles/CSS").Include( 
                "~/Assets/Styles/bootstrap.css", 
                "~/Assets/Styles/bootstrap-material-design.css",
                "~/Assets/Styles/ui-bootstrap-csp.css",
                "~/Assets/Styles/material-icons.css",
                "~/Assets/Styles/angular-block-ui.css",
                "~/Assets/Styles/ngDialog.css",
                "~/Assets/Styles/ngDialog-theme-default.css", 
                "~/Assets/Styles/Common.css",
                "~/Assets/Styles/Styles.css"
                ));


            BundleTable.EnableOptimizations = false;
        }
    }
}

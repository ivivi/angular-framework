﻿using System.Web;
using System.Web.Mvc;
using App.Web.Filter;

namespace App.Web
{
    public class FilterConfig
    {
        /// <summary>
        /// Mvc filter
        /// </summary>
        /// <param name="filters"></param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new ErrorForMvcAttribute());
            filters.Add(new AuthorizeForMvcAttribute()); 
        }
    }
}

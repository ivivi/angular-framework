﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace App.Web.References
{
    public static class ReferencedAssemblies
    {
        public static Assembly Services {
            get {
                return Assembly.Load("App.Service");
            }
        }
        public static Assembly Repositories
        {
            get
            {
                return Assembly.Load("App.Data");
            }
        }
        public static Assembly Domain
        {
            get
            {
                return Assembly.Load("App.Core");
            }
        }

        public static Assembly Cache
        {
            get
            {
                return Assembly.Load("App.Cache");
            }
        }

        public static Assembly Dto
        {
            get
            {
                return Assembly.Load("App.Dto");
            }
        }
        public static Assembly Web
        {
            get
            {
                return Assembly.Load("App.Web");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http.Filters;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Data.Entity.Validation;

namespace App.Web.Filter
{
    public class ErrorForWebApiAttribute: ExceptionFilterAttribute
    { 
        public override void OnException(HttpActionExecutedContext context)
        {
            string httpMethod = context.Request.Method.Method;
            var message = new System.Text.StringBuilder(); 
            message.AppendLine("HTTP-Method:" + httpMethod); 
            if (context.Request.Content.IsFormData())
            {
                message.AppendLine("参数信息:");
                System.Collections.Specialized.NameValueCollection formDate = context.Request.Content.ReadAsFormDataAsync().Result;
                foreach (var item in formDate.AllKeys)
                {
                    if (String.IsNullOrEmpty(item))
                        continue;
                    string value = formDate[item]; 
                    if (!String.IsNullOrEmpty(value) && value.Length > 1000)
                    {
                        value = value.Substring(0, 1000);
                    }
                    message.AppendLine(item + ":" + value);
                }
            }

            if (context.Exception is DbEntityValidationException)
            {
                var error = context.Exception as DbEntityValidationException; 
                message.AppendLine(String.Empty);
                message.AppendLine(value: "实体异常信息：");
                foreach (var item in error.EntityValidationErrors)
                {
                    string fieldErrors = String.Join(",", item.ValidationErrors.Select(x => String.Format("{0}:{1}", x.PropertyName, x.ErrorMessage)));
                    message.AppendLine(fieldErrors);
                } 
            }

             
            Core.Logging.ILogger iLogger = DependencyResolver.Current.GetService<App.Core.Logging.ILogger>();
            iLogger.Log(new Exception(message.ToString(), context.Exception));

            base.OnException(context);
        } 
    }
}
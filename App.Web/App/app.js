﻿(function(){
    'use strict';

    var app = angular.module('app', ['ngAnimate','pagination','ngFileUpload','blockUI','jcs-autoValidate','ngDialog', 'ngCookies', 'ngResource', 'ngRoute', 'ngSanitize', 'ngTouch', 'ui.bootstrap']); 
    app.directive('appHeader', function () {
        return {
            priority: 99999,
            restrict: 'AE',
            templateUrl: '/App/Templates/Shared/header.html', 
            controller: function ($scope,AuthService,tokenHandler) {
                $scope.logout = function () {
                    AuthService.logOut();
                    window.location.href = '/App/login.html';
                };
                $scope.LoginName = tokenHandler.getLoginName();
            }
        };
    });
    app.directive('appCopyriths', function () {
        return {
            priority: 99999,
            restrict: 'AE',
            templateUrl: '/App/Templates/Shared/copyriths.html', 
            controller: function ($scope) {
                $scope.Year = (new Date).getFullYear();
            }
        };
    });
    app.directive('appLefter', function () {
        return {
            priority: 99999,
            restrict: 'AE',
            templateUrl: '/App/Templates/Shared/lefter.html',
            controller: function ($scope, $http) {
                 
                $scope.menu_Click = function (menu, $index) {
                    if (menu.ChildMenus) {
                        for (var _indexOf = 0; _indexOf < $scope.Menus.length; _indexOf++) {
                            $scope.Menus[_indexOf].Selected = false;
                        }
                        menu.Selected = true;
                        menu.IsShowChildren = !menu.IsShowChildren;
                    }
                    else {
                        window.location.href = menu.url;
                    } 
                };

                $http({ 'method': 'GET', 'url': '/Api/Account/GetMenus', 'params': {'Date':'No'} }).success(function(getMenuResults){
                    var menuArr = [];

                    if (getMenuResults && getMenuResults.length > 0)
                    {
                        for (var _indexOfMenu = 0; _indexOfMenu < getMenuResults.length; _indexOfMenu++)
                        {
                            var _menu = getMenuResults[_indexOfMenu];
                            _menu.IsShowChildren  = false;
                            if (!_menu.parentId || _menu.parentId == 0)
                            {
                                menuArr.push(_menu);
                            }
                        }

                        for (var _indexOfMenu = 0; _indexOfMenu < getMenuResults.length; _indexOfMenu++) {
                            var _menu = getMenuResults[_indexOfMenu];
                            if (_menu.parentId && _menu.parentId > 0)
                            {
                                for (var _indexOfParentMenu = 0; _indexOfParentMenu < menuArr.length; _indexOfParentMenu++) {
                                    var _parentMenu = menuArr[_indexOfParentMenu];
                                    if (_parentMenu.id == _menu.parentId)
                                    {
                                        _parentMenu.url = 'javascript:void(0)';
                                        _parentMenu.ChildMenus = _parentMenu.ChildMenus || [];
                                        _parentMenu.ChildMenus.push(_menu);
                                        break;
                                    } 
                                }
                            }
                        }

                        menuArr[0].Selected = true;
                    }

                    $scope.Menus = menuArr;
                });
            }
        };
    });
     
    app.config(['blockUIConfig',function(blockUIConfig){
        blockUIConfig.message = '正在处理，请稍后！'; 
    }]);
      
    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('xmlHttpInteceptor');
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }
        $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    }]);
    function errorHandler(status, message) {
        var scope = angular.element($('html')).scope();
        scope.errorHandler(status, message);
    }; 
})();

$(document).ready(function () {
    $.material.init();
});

angular.autoValidate.errorMessages['default'] = {
    "defaultMsg": "请为 {0} 增加错误信息",
    "email": "请输入合法的电子邮箱地址",
    "minlength": "请输入至少 {0} 字符",
    "maxlength": "最多只允许输入 {0} 字符",
    "min": "请输入 {0} 允许的最小数值",
    "max": "请输入 {0} 允许的最大数值",
    "required": "这个字段是必须的",
    "date": "请输入合法日期",
    "pattern": "请确认输入信息符合规则 {0}",
    "number": "请输入一个合法数字",
    "url": "请输入正确的网址，如: http(s)://www.google.com"
};
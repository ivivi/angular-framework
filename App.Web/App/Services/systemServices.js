﻿(function () {
    'use strict';

    //一级菜单
    angular.module('app').factory('menuService', menuService);
    menuService.$inject = ['ngDialog']; 
    function menuService(ngDialog) {
        var _menuFactory = {};

        //编辑 - 添加
        var _edit = function (options) {
            options = options || {};
            ngDialog.open({
                template: '/App/Templates/System/MenuEdit.html',
                className: 'ngdialog-theme-default ngDialog-9 ngAlert',
                controller: ['$scope', 'Pagination', '$http', 'msgService', function ($scope, Pagination, $http, msgService) {
                    $scope.Id = options.Id;
                    $scope.sortNo = 0;
                    //加载模型数据
                    if ($scope.Id && $scope.Id != '') {
                        $http({ 'url': '/Api/System/GetMenu', 'method': 'GET', 'params': { 'Id': $scope.Id } }).success(function (results) {
                            if (results && results.isAccess) {
                                $scope.title = results.model.title;
                                $scope.icon = results.model.icon;
                                $scope.url = results.model.url;
                                $scope.view = results.model.view;
                                $scope.sortNo = results.model.sortNo;
                                $scope.parentId = results.model.parentId;
                            }
                        });
                    }

                    //保存
                    $scope.btnSubmit_Click = function ($valid) {
                        if (!$valid)
                            return;
                        var postParam = {
                            'Id': $scope.Id,
                            'Title': $scope.title,
                            'Icon': $scope.icon,
                            'URL': $scope.url,
                            'ParentId': $scope.parentId,
                            'View': $scope.view,
                            'SortNo': $scope.sortNo
                        };

                        $http.post('/Api/System/MenuEdit', postParam).success(function (results) {
                            if (!results.isAccess) {
                                msgService.alert({ 'message': '请检查表单！' });
                            }
                            else {
                                msgService.alert({
                                    'message': '保存成功！', 'okButtonClick': function () {
                                        if (options.closeCallback) {
                                            options.closeCallback({isAccess:true});
                                        }
                                        $scope.closeThisDialog();
                                    }
                                });
                            }
                        });
                    };

                    $scope.btnCancel_Click = function () {
                        $scope.closeThisDialog();
                    };
                }]
            }); 
        };

        //下级菜单列表
        var _level2 = function (options) {
            options = options || {};
            ngDialog.open({
                template: '/App/Templates/System/MenuLevel2Index.html',
                className: 'ngdialog-theme-default ngDialog-9 ngAlert',
                controller: ['$scope', 'Pagination', '$http', 'msgService', 'menuLevel2Service', function ($scope, Pagination, $http, msgService, menuLevel2Service) {
                    $scope.ParentId = options.Id;

                    //分页条
                    $scope.pager = new Pagination({
                        start: 0, limit: 10, total: 0, page: 1
                    });
                    $scope.pageSizeList = [10, 20, 50, 100];

                    var getMenuList = function () {
                        var postPara = {
                            'title': $scope.Title,
                            'parentId':$scope.ParentId,
                            'pageIndex': $scope.pager.page,
                            'pageSize': $scope.pager.limit
                        };
                        $http({ 'method': 'GET', url: '/Api/System/GetMenuList', params: postPara }).success(function (results) {
                            if (results) {
                                $scope.pager.set({ total: results.total });
                                $scope.menuList = results.list;
                            }
                        });
                    };
                    getMenuList();

                    $scope.btnSearch_Click = function () {
                        $scope.pager.set({ start: 0 });
                        $scope.pager.set({ page: 1 });
                        $scope.pager.set({ total: 0 });
                        getMenuList();
                    };

                    //添加按钮
                    $scope.btnAdd_Click = function (id) {
                        menuLevel2Service.edit({
                            'ParentId':$scope.ParentId,
                            'Id': id,
                            'closeCallback': function (results) {
                                if (results && results.isAccess) {
                                    getMenuList();
                                }
                            }
                        });
                    };

                    //删除按钮
                    $scope.btnDelete_Click = function (id) {
                        $http({ 'url': '/Api/System/MenuDelete', 'method': 'GET', 'params': { 'Id': id } }).success(function (results) {
                            if (results && results.isAccess) {
                                getMenuList();
                            }
                        });
                    };
                     
                }]
            });
        };
        
        _menuFactory.edit = _edit;
        _menuFactory.level2 = _level2;
        return _menuFactory;
    }

    //二级菜单
    angular.module('app').factory('menuLevel2Service', menuLevel2Service);
    menuLevel2Service.$inject = ['ngDialog'];
    function menuLevel2Service(ngDialog)
    {
        var _menuLevel2Factory = {};
        
        //编辑 - 添加 二级菜单
        var _edit = function (options) {
            options = options || {};
            ngDialog.open({
                template: '/App/Templates/System/MenuLevel2Edit.html',
                className: 'ngdialog-theme-default ngDialog-9 ngAlert',
                controller: ['$scope', 'Pagination', '$http', 'msgService', function ($scope, Pagination, $http, msgService) {
                    $scope.Id = options.Id;
                    $scope.parentId = options.ParentId;
                    $scope.sortNo = 0;

                    //加载模型数据
                    if ($scope.Id && $scope.Id != '') {
                        $http({ 'url': '/Api/System/GetMenu', 'method': 'GET', 'params': { 'Id': $scope.Id } }).success(function (results) {
                            if (results && results.isAccess) {
                                $scope.title = results.model.title;
                                $scope.icon = results.model.icon;
                                $scope.url = results.model.url;
                                $scope.view = results.model.view;
                                $scope.sortNo = results.model.sortNo;
                                $scope.parentId = results.model.parentId;
                            }
                        });
                    }

                    //保存
                    $scope.btnSubmit_Click = function ($valid) {
                        if (!$valid)
                            return;

                        var postParam = {
                            'Id': $scope.Id,
                            'Title': $scope.title,
                            'Icon': $scope.icon,
                            'URL': $scope.url,
                            'ParentId': $scope.parentId,
                            'View': $scope.view,
                            'SortNo': $scope.sortNo
                        };

                        $http.post('/Api/System/MenuEdit', postParam).success(function (results) {
                            if (!results.isAccess) {
                                msgService.alert({ 'message': '请检查表单！' });
                            }
                            else {
                                msgService.alert({
                                    'message': '保存成功！', 'okButtonClick': function () {
                                        if (options.closeCallback) {
                                            options.closeCallback({ isAccess: true });
                                        }
                                        $scope.closeThisDialog();
                                    }
                                });
                            }
                        });
                    };

                    $scope.btnCancel_Click = function () {
                        $scope.closeThisDialog();
                    };
                }]
            });
        };

        _menuLevel2Factory.edit = _edit;
        return _menuLevel2Factory;
    }

    //用户
    angular.module('app').factory('userService', userService);
    userService.$inject = ['ngDialog'];
    function userService(ngDialog) {
        var _userFactory = {};

        //用户 - 多选
        var _multiSelector = function (options) {
            ngDialog.open({
                template: '/App/Templates/System/UserMultiSelector.html',
                className: 'ngdialog-theme-default ngDialog-9 ngAlert',
                controller: ['$scope', 'Pagination', '$http', 'msgService', function ($scope, Pagination, $http, msgService) { 
                    //分页条
                    $scope.pager = new Pagination({
                        start: 0, limit: 10, total: 0, page: 1
                    });
                    $scope.pageSizeList = [10, 20, 50, 100]; 
                    $scope.getUserList = function () {
                        $http({ 'url': '/Api/System/GetUserList', 'method': 'GET', 'params': { 'userName': $scope.UserName } }).success(function (results) {
                            if (results && results.isAccess) {
                                $scope.pager.set({ total: results.total });
                                $scope.userList = results.list;
                            }
                        });
                    }; 
                    $scope.btnSelectAll_Click = function () {  
                        if ($scope.userList && $scope.userList.length > 0)
                        {
                            for (var i = 0; i < $scope.userList.length; i++) {
                                $scope.userList[i].Selected = $scope.SelectedAll;
                            }
                        }
                    };
                    $scope.btnSelected_Click = function () {
                        var jsonArr = [];
                        for (var i = 0; i < $scope.userList.length; i++) {
                            jsonArr.push($scope.userList[i].id);
                        }
                        if (options && options.onCloseCallback)
                        {
                            options.onCloseCallback(jsonArr);
                        }
                        $scope.closeThisDialog();
                    };
                }]
            });
        };

        var _edit = function (options) {
            options = options || {};
            ngDialog.open({
                template: '/App/Templates/System/UserEdit.html',
                className: 'ngdialog-theme-default ngDialog-9 ngAlert',
                controller: ['$scope', 'Pagination', '$http', 'msgService', function ($scope, Pagination, $http, msgService) {
                    
                    $scope.Id = options.Id;
                    $scope.getUser = function () {
                        if ($scope.Id)
                        {
                            $http({ 'url': '/Api/System/GetUser', 'method': 'GET', 'params': { 'Id': $scope.Id } }).success(function (results) {
                                if (results && results.isAccess) {
                                    $scope.model = results.model;
                                    $scope.id = results.model.id;
                                    $scope.userName = results.model.userName;
                                    $scope.email = results.model.email;
                                    $scope.phoneNumber = results.model.phoneNumber;
                                }
                            });
                        }
                    };

                    $scope.btnSave_Click = function ($valid) {
                        if (!$valid)
                            return;

                        var postParam = {
                            'Id': $scope.Id,
                            'userName': $scope.userName,
                            'email': $scope.email,
                            'phoneNumber': $scope.phoneNumber
                        };

                        $http.post('/Api/System/UserEdit', postParam).success(function (results) {
                            if (!results.isAccess) {
                                msgService.alert({ 'message': '请检查表单！' });
                            }
                            else {
                                msgService.alert({
                                    'message': '保存成功！', 'okButtonClick': function () {
                                        if (options.onCloseCallback) {
                                            options.onCloseCallback({ isAccess: true });
                                        }
                                        $scope.closeThisDialog();
                                    }
                                });
                            }
                        });
                    };

                    $scope.btnCancel_Click = function () {
                        $scope.closeThisDialog();
                    };

                }]
            });
        };

        _userFactory.multiSelctor = _multiSelector;
        _userFactory.edit = _edit;
        return _userFactory;
    }

    //角色
    angular.module('app').factory('roleService', roleService);
    roleService.$inject = ['ngDialog'];
    function roleService(ngDialog) {
        var _roleFactory = {};

        //编辑
        var _edit = function (options) {
            options = options || {};
            ngDialog.open({
                template: '/App/Templates/System/RoleEdit.html',
                className: 'ngdialog-theme-default ngDialog-9 ngAlert',
                controller: ['$scope', 'Pagination', '$http', 'msgService', function ($scope, Pagination, $http, msgService) {
                    $scope.Id = options.Id;

                    //加载模型数据
                    if ($scope.Id && $scope.Id != '') {
                        $http({ 'url': '/Api/System/GetRole', 'method': 'GET', 'params': { 'Id': $scope.Id } }).success(function (results) {
                            if (results && results.isAccess) {
                                $scope.name = results.model.name;
                                $scope.modulePermissions = results.model.modulePermissions;
                            }
                        });
                    }

                    //保存
                    $scope.btnSubmit_Click = function ($valid) {
                        if (!$valid)
                            return;
                        var postParam = {
                            'Id': $scope.Id,
                            'name': $scope.name,
                            'modulePermissions': $scope.modulePermissions
                        };

                        $http.post('/Api/System/RoleEdit', postParam).success(function (results) {
                            if (!results.isAccess) {
                                msgService.alert({ 'message': '请检查表单！' });
                            }
                            else {
                                msgService.alert({
                                    'message': '保存成功！', 'okButtonClick': function () {
                                        if (options.closeCallback) {
                                            options.closeCallback({ isAccess: true });
                                        }
                                        $scope.closeThisDialog();
                                    }
                                });
                            }
                        });
                    };

                    $scope.btnCancel_Click = function () {
                        $scope.closeThisDialog();
                    };
                }]
            });
        };

        //用户列表
        var _users = function (options) {
            options = options || {};
            ngDialog.open({
                template: '/App/Templates/System/RoleUsers.html',
                className: 'ngdialog-theme-default ngDialog-9 ngAlert',
                controller: ['$scope', 'Pagination', '$http', 'msgService', 'userService', function ($scope, Pagination, $http, msgService, userService) {
                    $scope.Id = options.Id;  
                    //分页条
                    $scope.pager = new Pagination({
                        start: 0, limit: 10, total: 0, page: 1
                    });
                    $scope.pageSizeList = [10, 20, 50, 100];

                    //获取角色-用户列表
                    $scope.getRoleUserList = function () {
                        $http({ 'url': '/Api/System/GetRoleUserList', 'method': 'GET', 'params': { 'Id': $scope.Id } }).success(function (results) {
                            if (results && results.isAccess) {
                                $scope.pager.set({ total: results.total });
                                $scope.userList = results.list;
                            }
                        });
                    };

                    //添加角色用户按钮事件
                    $scope.btnAdd_Click = function () {
                        userService.multiSelctor({
                            'onCloseCallback': function (results) {
                                if (results && results.length > 0)
                                {
                                    var postParam = {
                                        'Id': $scope.Id,
                                        'UserId': results.join(',')
                                    };

                                    $http({ 'url': '/Api/System/AddRoleUser', 'method': 'GET', 'params': postParam }).success(function (results) {
                                        if (!results.isAccess) {
                                            msgService.alert({ 'message': '请检查表单！' });
                                        }
                                        else {
                                            $scope.getRoleUserList();
                                            msgService.alert({
                                                'message': '添加成功！'
                                            });
                                        }
                                    }); 
                                }
                            }
                        });
                    };

                    //删除角色用户事件
                    $scope.btnRemove_Click = function (userId) {
                        $http({ 'url': '/Api/System/RemoveRoleUser', 'method': 'GET', 'params': { 'Id': $scope.Id,'UserId':userId } }).success(function (results) {
                            if (results && results.isAccess) {
                                $scope.pager.set({ start: 0,total:0,page:1});
                                $scope.getRoleUserList();
                            }
                        });
                    };

                }]
            });
        };

        //权限配置
        var _permission = function (options) {
            options = options || {};
            ngDialog.open({
                template: '/App/Templates/System/RolePermission.html',
                className: 'ngdialog-theme-default ngDialog-9 ngAlert',
                controller: ['$scope', 'Pagination', '$http', 'msgService', function ($scope, Pagination, $http, msgService) {
                    $scope.Id = options.Id;
                    $scope.init = function () {
                        $http({ 'url': '/Api/System/GetRolePermission', 'method': 'GET', 'params': { 'Id': $scope.Id } }).success(function (results) {
                            if (results && results.isAccess) {
                                $scope.modulePermissions = results.modulePermissions;
                            }
                        });
                    };
                    $scope.btnCancel_Click = function () {
                        $scope.closeThisDialog();
                    };
                    $scope.btnSubmit_Click = function () {
                        var moduleCount = $scope.modulePermissions.length;
                        
                        var modulePerissionsSelected = [];
                        for (var indexOfModule = 0; indexOfModule < moduleCount; indexOfModule++) {
                            var modulePermission = $scope.modulePermissions[indexOfModule];
                            var module = modulePermission.module;
                            var permissions = modulePermission.permissions;
                            var permissionSelectedArr = [];
                            for (var indexOfPermission = 0; indexOfPermission < permissions.length; indexOfPermission++) {
                                var permission = permissions[indexOfPermission];
                                if (permission.isSelected)
                                {
                                    permissionSelectedArr.push(permission.permission);
                                }
                            }

                            if (permissionSelectedArr.length > 0)
                                modulePerissionsSelected.push(module + '=' + permissionSelectedArr.join(','));

                        }

                        var modulePermissionStr = modulePerissionsSelected.join(';');
                        var postParam = {
                            'Id': $scope.Id, 
                            'ModulePermissions': modulePermissionStr
                        };

                        $http.post('/Api/System/EditRolePermission', postParam).success(function (results) {
                            if (!results.isAccess) {
                                msgService.alert({ 'message': '请检查表单！' });
                            }
                            else {
                                msgService.alert({
                                    'message': '保存成功！', 'okButtonClick': function () {
                                        if (options.onCloseCallback) {
                                            options.onCloseCallback({ isAccess: true });
                                        }
                                        $scope.closeThisDialog();
                                    }
                                });
                            }
                        });
                    };
                }]
            });
        };

        _roleFactory.edit = _edit;
        _roleFactory.users = _users;
        _roleFactory.permission = _permission;
        return _roleFactory;
    }

    //模块
    angular.module('app').factory('moduleService', moduleService);
    moduleService.$inject = ['ngDialog'];
    function moduleService(ngDialog) {
        var _moduleFactory = {};
        var _edit = function (options) {

            ngDialog.open({
                template: '/App/Templates/System/ModuleEdit.html',
                className: 'ngdialog-theme-default ngDialog-9 ngAlert',
                controller: ['$scope', 'Pagination', '$http', 'msgService', function ($scope, Pagination, $http, msgService) {
                    $scope.Id = options.Id;
                    $scope.getModule = function () {
                        if ($scope.Id)
                        {
                            $http({ 'url': '/Api/System/GetModule', 'method': 'GET', 'params': { 'Id': $scope.Id } }).success(function (results) {
                                if (results && results.isAccess) {
                                    $scope.module = results.model.module;
                                    $scope.permissions = results.model.permissions;
                                }
                            });
                        }
                    };
                     
                    //保存
                    $scope.btnSubmit_Click = function ($valid) {
                        if (!$valid)
                            return;
                        var postParam = {
                            'Id': $scope.Id,
                            'module': $scope.module,
                            'permissions': $scope.permissions
                        };

                        $http.post('/Api/System/ModuleEdit', postParam).success(function (results) {
                            if (!results.isAccess) {
                                msgService.alert({ 'message': '请检查表单！' });
                            }
                            else {
                                msgService.alert({
                                    'message': '保存成功！', 'okButtonClick': function () {
                                        if (options.onCloseCallback) {
                                            options.onCloseCallback({ isAccess: true });
                                        }
                                        $scope.closeThisDialog();
                                    }
                                });
                            }
                        });
                    };

                    $scope.btnCancel_Click = function () {
                        $scope.closeThisDialog();
                    };
                }]
            });

        };

        _moduleFactory.edit = _edit;
        return _moduleFactory;
    };
})();
﻿(function(){
    'use strict';
    angular
        .module('app')
        .factory('vendorSelectorService', vendorSelectorService);
    vendorSelectorService.$inject = ['ngDialog'];

    function vendorSelectorService(ngDialog) {
        var _vendorSelectorFactory = {};

        var _singleSelector = function(options){
            options = options || {};
            ngDialog.open({
                template: '/App/Templates/Shared/VendorSingleSelector.html',
                className: 'ngdialog-theme-default ngDialog-9 ngAlert',
                controller: ['$scope', 'Pagination', '$http', function ($scope, Pagination, $http) {
                    //分页条
                    $scope.pager = new Pagination({
                        start: 0, limit: 10, total: 0, page: 1
                    });
                    $scope.pageSizeList = [10, 20, 50, 100];
                    $scope.pageChange = function () {
                        if ($scope.pager.page > $scope.pager.pages) {
                            $scope.pager.page = $scope.pager.pages;
                        }
                        getVendorList();
                    };
                    $scope.pageSizeChange = function () {
                        getVendorList();
                    };

                    //加载表单数据
                    var getVendorList = function () {
                        var postPara = {
                            'vendorName': $scope.VendorName,
                            'combination': $scope.Combination,
                            'pageIndex': $scope.pager.page,
                            'pageSize': $scope.pager.limit
                        };
                        $http({ 'method': 'GET', url: '/Api/Vendor/GetVendorList', params: postPara }).success(function (results) {
                            if (results) {
                                $scope.pager.set({ total: results.total });
                                $scope.vendorList = results.list;
                            }
                        });
                    };
                    getVendorList();

                    //查询事件 
                    $scope.btnSearch_Click = function () {
                        getVendorList();
                    };

                    $scope.btnSelect_Click = function (vendor) { 
                        if (options.closeCallback) {
                            options.closeCallback({ 'id': vendor.id, 'companyName': vendor.companyName, 'combination': vendor.combination, 'companyAddress': vendor.companyAddress, 'contactPersion': vendor.contactPersion });
                        }
                        $scope.closeThisDialog();
                    };
                }]
            });

        };

        _vendorSelectorFactory.singleSelector = _singleSelector; 
        return _vendorSelectorFactory;
    }
})();
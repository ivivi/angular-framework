﻿ 
(function () {
    'use strict';

    angular
        .module('app')
        .factory('msgService', msgService);

    msgService.$inject = ['ngDialog'];

    function msgService(ngDialog) {
         
        var msgServiceFactory = {};

        var _alert = function(option){
            option = option || {};

            ngDialog.open({
                template: '/App/Templates/Shared/alert.html',
                className: 'ngdialog-theme-default ngDialog-6 ngAlert',
                controller: ['$scope', function($scope) {
                    $scope.message = option.message || '警告！';
                    $scope.okButtonText = option.okButtonText || '确定';
                    $scope.okButtonShow = true;
                    $scope.okButtonClick = function(){
                        if(option.okButtonClick)
                        {
                            option.okButtonClick();
                        }
                        $scope.closeThisDialog();
                    }; 
                }]
            });
        };

        var _confirm = function(option){
            option = option || {};

            ngDialog.open({
                template: '/App/Templates/Shared/confirm.html',
                className: 'ngdialog-theme-default ngDialog-6 ngAlert',
                controller: ['$scope', function($scope) {
                    $scope.message = option.message || '确认？';
                    $scope.okButtonText = option.okButtonText || '确定';
                    $scope.cancelButtonText = option.cancelButtonText || '取消';
                    $scope.okButtonClick = function(){
                        if(option.okButtonClick)
                        {
                            option.okButtonClick();
                        }
                        $scope.closeThisDialog();
                    };
                    $scope.cancelButtonClick = function () {
                        if (option.cancelButtonClick) {
                            option.cancelButtonClick();
                        }
                        $scope.closeThisDialog();
                    };
                }]
            });
        };

        msgServiceFactory.alert = _alert;
        msgServiceFactory.confirm = _confirm;
        return msgServiceFactory;
    };
})();
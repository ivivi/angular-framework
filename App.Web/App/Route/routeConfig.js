﻿(function () {
    'use strict';

    angular.module('app')
        .config(['$routeProvider', function ($routeProvider) {
            //首页
            $routeProvider.when('/', {
                templateUrl: '/App/Templates/Home/Index.html',
                requiresLogin: true,
                controller: 'homeIndexController'
            })

            //供应商管理
            .when('/Vendor/Vendor', {
                templateUrl: '/App/Templates/Vendor/VendorIndex.html',
                requiresLogin: true,
                controller: 'vendorIndexController'
            })
            .when('/Vendor/VendorEdit/:Id?', {
                templateUrl: '/App/Templates/Vendor/VendorEdit.html',
                requiresLogin: true,
                controller: 'vendorEditController'
            })
            
            //顾问管理
            .when('/Vendor/Consultant', {
                templateUrl: '/App/Templates/Vendor/ConsultantIndex.html',
                requiresLogin: true,
                controller: 'consultantIndexController'
            })
            .when('/Vendor/ConsultantEdit:Id?', {
                templateUrl: '/App/Templates/Vendor/ConsultantEdit.html',
                requiresLogin: true,
                controller: 'consultantEditController'
            })

            //表单模块
            .when('/Form/Icons', {
                templateUrl: '/App/Templates/Form/Icons.html',
                requiresLogin: true,
                controller: 'formIconsController'
            })
            .when('/Form/Buttons', {
                templateUrl: '/App/Templates/Form/Buttons.html',
                requiresLogin: true,
                controller: 'formButtonsController'
            })
            .when('/Form/Texts', {
                templateUrl: '/App/Templates/Form/Texts.html',
                requiresLogin: true,
                controller: 'formTextsController'
            })
            .when('/Form/Tables', {
                templateUrl: '/App/Templates/Form/Tables.html',
                requiresLogin: true,
                controller: 'formTablesController'
            })
            .when('/Form/Inputs', {
                templateUrl: '/App/Templates/Form/Inputs.html',
                requiresLogin: true,
                controller: 'formInputsController'
            })
            .when('/Form/Radios', {
                templateUrl: '/App/Templates/Form/Radios.html',
                requiresLogin: true,
                controller: 'formRadiosController'
            })
            .when('/Form/Checkboxs', {
                templateUrl: '/App/Templates/Form/Checkboxs.html',
                requiresLogin: true,
                controller: 'formCheckboxsController'
            })
            
            //系统管理 
            .when('/System/Menu', {
                templateUrl: '/App/Templates/System/MenuIndex.html',
                requiresLogin: true,
                controller: 'menuIndexController'
            }) 
            .when('/System/Role', {
                templateUrl: '/App/Templates/System/RoleIndex.html',
                requiresLogin: true,
                controller: 'roleIndexController'
            })
            .when('/System/User', {
                templateUrl: '/App/Templates/System/UserIndex.html',
                requiresLogin: true,
                controller: 'userIndexController'
            })
            .when('/System/Module', {
                templateUrl: '/App/Templates/System/Module.html',
                requiresLogin: true,
                controller: 'moduleIndexController'
            })

            //异常处理
          .otherwise({
              templateUrl: '/App/Templates/Shared/_404.html'
          })
        }])
        .run(checkAuthentication);

    checkAuthentication.$inject = ['$rootScope', '$location', 'tokenHandler'];
    function checkAuthentication($rootScope,  $location, tokenHandler) {
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            var requiresLogin = next.requiresLogin || false;
            if (requiresLogin) {

                var loggedIn = tokenHandler.hasLoginToken();

                if (!loggedIn) {
                    window.location.href = '/Home/Login';
                }
            }
        }); 
    }
})();
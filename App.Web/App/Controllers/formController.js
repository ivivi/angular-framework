﻿(function () {
    'use strict';

    //Icon - Index
    angular.module('app').controller('formIconsController', formIconsController);
    formIconsController.$inject = ['$scope'];
    function formIconsController($scope) {
        $scope.cqrdIcons = [
            { icon: 'add', show: false },
            { icon: 'add_box', show: false },
            { icon: 'add_circle', show: false },
            { icon: 'add_circle_outline', show: false },
            { icon: 'add_alarm', show: false }, 
            { icon: 'alarm_add', show: false },
            { icon: 'note_add', show: false },
            { icon: 'add_alert', show: false },
            { icon: 'add_to_queue', show: false },
            { icon: 'delete', show: false },
            { icon: 'find_in_page', show: false },
            { icon: 'find_replace', show: false }, 
            { icon: 'pageview', show: false },
            { icon: 'search', show: false },
            { icon: 'zoom_in', show: false },
            { icon: 'zoom_out', show: false },
            { icon: 'clear', show: false },
            { icon: 'create', show: false },
            { icon: 'remove_circle', show: false },
            { icon: 'remove_circle_outline', show: false },
            { icon: 'border_color', show: false }
        ];

        $scope.personIcons = [
            { icon: 'accessibility_black', show: false },
            { icon: 'accessible', show: false },
            { icon: 'face', show: false },
            { icon: 'account_box', show: false },
            { icon: 'contacts', show: false },
            { icon: 'account_circle', show: false },
            { icon: 'android', show: false }, 
            { icon: 'contact_phone', show: false },
            { icon: 'insert_emoticon_black', show: false },
            { icon: 'perm_identity_black', show: false },
            { icon: 'supervisor_account_black',show:false }
        ];

        $scope.directionIcons = [
        { icon: 'input', show: false },
        { icon: 'restore', show: false },
        { icon: 'all_out', show: false },
        { icon: 'assignment_returned', show: false },
        { icon: 'assignment_return_black', show: false },
        { icon: 'assignment_turned_in_black', show: false },
        { icon: 'autorenew_black', show: false },
        { icon: 'backup_black', show: false },
        { icon: 'cached_black', show: false },
        { icon: 'check_circle_black', show: false },
        { icon: 'code_black', show: false },
        { icon: 'compare_arrows_black', show: false },
        { icon: 'copyright_black', show: false },
        { icon: 'done_black', show: false },
        { icon: 'done_all_black', show: false },
        { icon: 'donut_large_black', show: false },
        { icon: 'get_app_black', show: false },
        { icon: 'hourglass_empty_black', show: false },
        { icon: 'call_made_black', show: false },
        { icon: 'call_merge_black', show: false },
        { icon: 'call_split_black', show: false },
        { icon: 'reply_black', show: false },
        { icon: 'reply_all_black', show: false },
        { icon: 'send_black', show: false },
        { icon: 'undo_black', show: false },
        { icon: 'cloud_download_black', show: false },
        { icon: 'trending_flat_black', show: false },
        { icon: 'trending_neutral_black', show: false },
        { icon: 'trending_down_black', show: false },
        { icon: 'trending_up_black', show: false },
        { icon: 'replay_black', show: false },
        { icon: 'keyboard_arrow_down_black', show: false },
        { icon: 'keyboard_arrow_left_black', show: false },
        { icon: 'keyboard_arrow_right_black', show: false },
        { icon: 'keyboard_arrow_up_black', show: false },
        { icon: 'keyboard_backspace_black', show: false },
        { icon: 'keyboard_capslock_black', show: false },
        { icon: 'refresh_black',show:false }
        ];
         
        $scope.toolsIcons = [
            { icon: 'account_balance_black', show: false },
            { icon: 'bug_report_black', show: false },
            { icon: 'build_black', show: false },
            { icon: 'camera_enhance_black', show: false },
            { icon: 'card_giftcard_black', show: false },
            { icon: 'card_membership_black', show: false },
            { icon: 'card_travel_black', show: false },
            { icon: 'change_history_black', show: false },
            { icon: 'exit_to_app_black', show: false },
            { icon: 'extension_black', show: false },
            { icon: 'explore_black', show: false },
            { icon: 'help_black', show: false },
            { icon: 'help_outline_black', show: false },
            { icon: 'highlight_off_black', show: false },
            { icon: 'highlight_remove_black', show: false },
            { icon: 'history_black', show: false },
            { icon: 'home_black', show: false },
            { icon: 'cloud_black', show: false },
            { icon: 'attachment_black', show: false },
            { icon: 'cloud_circle_black', show: false },
            { icon: 'cloud_done_black', show: false },
            { icon: 'cloud_download_black', show: false },
            { icon: 'bookmark_border_black', show: false },
            { icon: 'bookmark_black', show: false },
            { icon: 'lock_black', show: false },
            { icon: 'lock_open_black', show: false },
            { icon: 'perm_phone_msg_black', show: false },
            { icon: 'settings_black', show: false },
            { icon: 'settings_applications_black', show: false },
            { icon: 'star_rate_black', show: false },
            { icon: 'stars_black', show: false },
            { icon: 'thumb_down_black', show: false },
            { icon: 'thumb_up_black', show: false },
            { icon: 'touch_app_black', show: false },
            { icon: 'error_black', show: false },
            { icon: 'error_outline_black', show: false },
            { icon: 'mail_outline_black', show: false },
            { icon: 'flag_black', show: false },
            { icon: 'mail_black', show: false },
            { icon: 'check_box_black', show: false },
            { icon: 'check_box_outline_blank_black', show: false },
            { icon: 'indeterminate_check_box_black', show: false },
            { icon: 'radio_button_checked_black', show: false },
            { icon: 'radio_button_unchecked_black',show:false }
        ];

        $scope.listsIcons = [
            { icon: 'assessment', show: false },
            { icon: 'assignment', show: false },
            { icon: 'assignment_late', show: false },
            { icon: 'bookmark_black', show: false },
            { icon: 'bookmark_border_black', show: false },
            { icon: 'chrome_reader_mode_black', show: false },
            { icon: 'bookmark_outline_black', show: false },
            { icon: 'chrome_reader_mode_black', show: false },
            { icon: 'class_black', show: false },
            { icon: 'credit_card_black', show: false },
            { icon: 'description_black', show: false },
            { icon: 'dns_black', show: false },
            { icon: 'list_black', show: false },
            { icon: 'drag_handle_black', show: false },
            { icon: 'format_align_center_black', show: false },
            { icon: 'format_align_justify_black', show: false },
            { icon: 'format_align_left_black', show: false },
            { icon: 'format_align_right_black', show: false },
            { icon: 'format_line_spacing_black', show: false },
            { icon: 'format_list_bulleted_black', show: false },
            { icon: 'format_list_numbered_black',show:false }
        ];

        $scope.vidiosIcons = [ 
            { icon: 'mic_black', show: false },
            { icon: 'mic_none_black', show: false },
            { icon: 'mic_off_black', show: false },  
            { icon: 'movie_black', show: false },   
            { icon: 'pause_black', show: false },
            { icon: 'pause_circle_filled_black', show: false },
            { icon: 'pause_circle_outline_black', show: false },
            { icon: 'play_arrow_black', show: false },
            { icon: 'play_circle_filled_black', show: false },
            { icon: 'play_circle_filled_white_black', show: false },
            { icon: 'play_circle_fill_black', show: false },
            { icon: 'play_circle_outline_black', show: false },
            { icon: 'queue_mus3x_black', show: false },
            { icon: 'radio_black', show: false },
            { icon: 'skip_next_black', show: false },
            { icon: 'skip_previous_black', show: false },
            { icon: 'videocam_black', show: false },
            { icon: 'videocam_off_black', show: false },
            { icon: 'volume_down_black', show: false }
        ];
    }

    angular.module('app').controller('formButtonsController', formButtonsController);
    formButtonsController.$inject = ['$scope'];
    function formButtonsController($scope)
    {

    }

    angular.module('app').controller('formTextsController', formTextsController);
    formTextsController.$inject = ['$scope'];
    function formTextsController($scope) {

    }

    angular.module('app').controller('formTablesController', formTablesController);
    formTablesController.$inject = ['$scope', 'Pagination'];
    function formTablesController($scope,Pagination) {
        //分页条
        $scope.pager = new Pagination({
            start: 0, limit: 10, total: 0, page: 1
        });
        $scope.pageSizeList = [10, 20, 50, 100];
        $scope.pageChange = function () {
            if ($scope.pager.page > $scope.pager.pages) {
                $scope.pager.page = $scope.pager.pages;
            } 
        };
        $scope.pageSizeChange = function () {
             
        };
    }

    angular.module('app').controller('formInputsController', formInputsController);
    formInputsController.$inject = ['$scope'];
    function formInputsController($scope) {

    }

    angular.module('app').controller('formRadiosController', formRadiosController);
    formRadiosController.$inject = ['$scope'];
    function formRadiosController($scope) {

    }

    angular.module('app').controller('formCheckboxsController', formCheckboxsController);
    formCheckboxsController.$inject = ['$scope'];
    function formCheckboxsController($scope) {

    }
})();
﻿(function () {
    'use strict';

    //Home - Index
    angular.module('app').controller('homeIndexController', homeIndexController); 
    homeIndexController.$inject = ['$scope'];
    function homeIndexController($scope) {

    }

    //Home - Login
    angular.module('app').controller('homeLoginController', homeLoginController);
    homeLoginController.$inject = ['$scope', 'AuthService', 'blockUI'];
    function homeLoginController($scope, AuthService, blockUI) {

        $scope.loginData = {
            userName: "ivivi",
            password: "000000"
        };

        $scope.message = "";
        $scope.login = function () {

            AuthService.login($scope.loginData).then(
            function (response) {
                window.location.href = '/App/index.html#/';
            },
            function (data) {
                $scope.message = data.error_description;
            });
        };
    }
})();
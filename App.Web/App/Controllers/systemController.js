﻿(function () {
    'use strict';

    //菜单
    angular.module('app').controller('menuIndexController', menuIndexController);
    menuIndexController.$inject = ['$scope', '$http', 'Pagination', 'menuService', 'msgService'];
    function menuIndexController($scope, $http, Pagination, menuService, msgService) {
        //分页条
        $scope.pager = new Pagination({
            start: 0, limit: 10, total: 0, page: 1
        });
        $scope.pageSizeList = [10, 20, 50, 100];
        var getMenuList = function () {
            var postPara = {
                'title': $scope.Title, 
                'pageIndex': $scope.pager.page,
                'pageSize': $scope.pager.limit
            };
            $http({ 'method': 'GET', url: '/Api/System/GetMenuList', params: postPara }).success(function (results) {
                if (results) {
                    $scope.pager.set({ total: results.total });
                    $scope.menuList = results.list;
                }
            });
        };
        getMenuList();

        //添加按钮
        $scope.btnAdd_Click = function (id) {
            menuService.edit({
                'Id':id,
                'closeCallback': function (results) {
                    if (results && results.isAccess) {
                        getMenuList();
                    }
                }
            });
        };

        //删除按钮
        $scope.btnDelete_Click = function (id) {
            msgService.confirm({
                'message': '确定要删除?', 'okButtonClick': function () {
                    $http({ 'url': '/Api/System/MenuDelete', 'method': 'GET', 'params': { 'Id': id } }).success(function (results) {
                        if (results && results.isAccess) {
                            getMenuList();
                        }
                    });
                }
            }); 
        };

        //下级菜单
        $scope.btnSublevel_Click = function (id) {
            menuService.level2({
                'Id':id
            });
        };
    } 

    //角色
    angular.module('app').controller('roleIndexController', roleIndexController);
    roleIndexController.$inject = ['$scope', '$http', 'Pagination', 'roleService', 'msgService'];
    function roleIndexController($scope, $http, Pagination, roleService, msgService) {
        //分页条
        $scope.pager = new Pagination({
            start: 0, limit: 10, total: 0, page: 1
        });
        $scope.pageSizeList = [10, 20, 50, 100];
        var getRoleList = function () {
            var postPara = {
                'name': $scope.name,
                'pageIndex': $scope.pager.page,
                'pageSize': $scope.pager.limit
            };
            $http({ 'method': 'GET', url: '/Api/System/GetRoleList', params: postPara }).success(function (results) {
                if (results) {
                    $scope.pager.set({ total: results.total });
                    $scope.roleList = results.list;
                }
            });
        };
        getRoleList();

        //添加
        $scope.btnAdd_Click = function (id) {
            roleService.edit({
                'Id': id,
                'closeCallback': function (results) {
                    if (results && results.isAccess) {
                        getRoleList();
                    }
                }
            });
        };

        //删除角色
        $scope.btnDelete_Click = function (id) {
            msgService.confirm({
                'message': '确定要删除?', 'okButtonClick': function () {
                    $http({ 'url': '/Api/System/RoleDelete', 'method': 'GET', 'params': { 'Id': id } }).success(function (results) {
                        if (results && results.isAccess) {
                            getRoleList();
                        }
                    });
                }
            });
        };

        //角色-用户
        $scope.btnRoleUser_Click = function (id) {
            roleService.users({
                'Id': id
            });
        };

        //角色-权限
        $scope.btnPermission_Click = function (id) {
            roleService.permission({'Id':id});
        };
    }

    //用户
    angular.module('app').controller('userIndexController', userIndexController);
    userIndexController.$inject = ['$scope', '$http', 'Pagination', 'roleService', 'msgService', 'userService'];
    function userIndexController($scope, $http, Pagination, roleService, msgService, userService) {
        //分页条
        $scope.pager = new Pagination({
            start: 0, limit: 10, total: 0, page: 1
        });
        $scope.pageSizeList = [10, 20, 50, 100];
        $scope.getUserList = function () {
            var postPara = {
                'userName': $scope.name,
                'pageIndex': $scope.pager.page,
                'pageSize': $scope.pager.limit
            };
            $http({ 'method': 'GET', url: '/Api/System/GetUserList', params: postPara }).success(function (results) {
                if (results) {
                    $scope.pager.set({ total: results.total });
                    $scope.userList = results.list; 
                }
            });
        };
        $scope.btnSearch_Click = function () {
            $scope.pager.set({ total: 0, page: 1 });
            $scope.getUserList();
        };
        $scope.btnAdd_Click = function (id) {
            userService.edit({
                'Id': id,
                'onCloseCallback': function (results) {
                    if (results && results.isAccess) {
                        $scope.getUserList();
                    }
                }
            });
        };
    }

    // 模块
    angular.module('app').controller('moduleIndexController', moduleIndexController);
    moduleIndexController.$inject = ['$scope', '$http', 'Pagination', 'roleService', 'msgService', 'moduleService'];
    function moduleIndexController($scope, $http, Pagination, roleService, msgService, moduleService)
    {

        //获取模块列表
        $scope.getModuleList = function () {
            var postPara = {
                'module': $scope.module
            };
            $http({ 'method': 'GET', url: '/Api/System/GetModuleList', params: postPara }).success(function (results) {
                if (results) { 
                    $scope.moduleList = results.list;
                }
            });
        };

        //添加模块
        $scope.btnAdd_Click = function (id) {
            moduleService.edit({
                'Id':id,
                'onCloseCallback': function (results) {
                    if (results && results.isAccess)
                    {
                        $scope.getModuleList();
                    }
                }
            });
        };

        //删除模块
        $scope.btnDelete_Click = function (id) {
            msgService.confirm({
                'message': '确定要删除?', 'okButtonClick': function () {
                    $http({ 'url': '/Api/System/ModuleDelete', 'method': 'GET', 'params': { 'Id': id } }).success(function (results) {
                        if (results && results.isAccess) {
                            $scope.getModuleList();
                        }
                    });
                }
            });
        };
    }
})();
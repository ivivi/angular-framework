﻿(function () {
    'use strict';

    //Vendor-Index
    angular.module('app').controller('vendorIndexController', vendorIndexController);
    vendorIndexController.$inject = ['$scope', '$http', 'Pagination'];
    function vendorIndexController($scope, $http, Pagination) {

        //添加按钮事件
        $scope.btnAdd_Click = function () {
            window.location.href = '/#/Vendor/VendorEdit';
        };

        //分页条
        $scope.pager = new Pagination({
            start: 0, limit: 10, total: 0, page: 1
        });
        $scope.pageSizeList = [10, 20, 50, 100];
        $scope.pageChange = function () {
            if ($scope.pager.page > $scope.pager.pages) {
                $scope.pager.page = $scope.pager.pages;
            }
            getVendorList();
        };
        $scope.pageSizeChange = function () {
            getVendorList();
        };

        //加载表单数据
        var getVendorList = function () {
            var postPara = {
                'vendorName': $scope.VendorName,
                'combination': $scope.Combination,
                'pageIndex': $scope.pager.page,
                'pageSize': $scope.pager.limit
            };
            $http({ 'method': 'GET', url: '/Api/Vendor/GetVendorList', params: postPara }).success(function (results) {
                if (results) {
                    $scope.pager.set({ total: results.total });
                    $scope.vendorList = results.list;
                }
            });
        };
        getVendorList();

        //查询事件 
        $scope.btnSearch_Click = function () {
            getVendorList();
        };
    }

    //Vendor-Edit
    angular.module('app').controller('vendorEditController', vendorEditController);
    vendorEditController.$inject = ['$scope', '$routeParams', '$http', 'Pagination', 'Upload', 'msgService'];
    function vendorEditController($scope, $routeParams, $http, Pagination, Upload, msgService) {
        $scope.Id = $routeParams.Id;
        $scope.submitted = false;

        //加载模型数据
        if ($scope.Id && $scope.Id != '') {
            $http({ 'url': '/Api/Vendor/GetVendor', 'method': 'GET', 'params': { 'Id': $scope.Id } }).success(function (results) {
                if (results && results.isAccess) {
                    $scope.CompanyName = results.model.companyName;
                    $scope.Combination = results.model.combination;
                    $scope.CompanyAddress = results.model.companyAddress;
                    $scope.ContactPersion = results.model.contactPersion;
                    $scope.Tel = results.model.tel;
                    $scope.PostCode = results.model.postCode;
                    $scope.Email = results.model.email;
                    $scope.Remark = results.model.remark;

                    if (results.attachfileList && results.attachfileList.length > 0) {
                        $scope.Attachfile = results.attachfileList;
                    }
                }
            });
        }

        //取消
        $scope.btnCancel_Click = function () {
            window.location.href = '/#/Vendor/Vendor';
        };

        //上传附件
        $scope.fileUploadAttachfile_Select = function ($file) {
            try {
                $scope.UploadProcess = '0%';
                $scope.IsShowProcessbar = true;
                Upload.upload({
                    url: '/Api/Vendor/UploadVendorAttachfile',
                    data: {
                        file: $file
                    }
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.UploadProcess = progressPercentage + '%';
                }).success(function (data, status, headers, config) {
                    $scope.UploadAttachfile = '';
                    $scope.UploadProcess = '0%';
                    $scope.IsShowProcessbar = false;
                    $scope.Attachfile = $scope.Attachfile || [];
                    if (data && data.file)
                        $scope.Attachfile.push(data.file);
                });
            }
            catch (ex) {
                $scope.UploadAttachfile = '';
                UnMaskbody();
                console.log(ex);
            }
        };

        //删除附件
        $scope.btnDeleteAttachfile_Click = function ($index, file) {
            $scope.Attachfile.splice($index, 1);
        };

        //提交
        $scope.btnSubmit_Click = function () {
            if ($scope.Vendor.$valid) {
                var attachfileIdStr = '';
                if ($scope.Attachfile && $scope.Attachfile.length > 0) {
                    var attachIdArr = [];
                    for (var i = 0; i < $scope.Attachfile.length; i++) {
                        attachIdArr.push($scope.Attachfile[i].id);
                    }
                    attachfileIdStr = attachIdArr.join(',');
                }
                var postParam = {
                    'Id': $scope.Id,
                    'CompanyName': $scope.CompanyName,
                    'Combination': $scope.Combination,
                    'CompanyAddress': $scope.CompanyAddress,
                    'ContactPersion': $scope.ContactPersion,
                    'Tel': $scope.Tel,
                    'PostCode': $scope.PostCode,
                    'Email': $scope.Email,
                    'Remark': $scope.Remark,
                    'AttachfileId': attachfileIdStr
                };
                $http.post('/Api/Vendor/VendorEdit', postParam).success(function (result) {
                    if (!result.isAccess) {
                        msgService.alert({ 'message': '请检查表单！' });
                    }
                    else {
                        msgService.alert({
                            'message': '保存成功！', 'okButtonClick': function () {
                                window.location.href = '/#/Vendor/Vendor';
                            }
                        });
                    }
                });
            }
            else {
                $scope.Vendor.submitted = true;
            }
        }
    }

    //Consultant-Index
    angular.module('app').controller('consultantIndexController', consultantIndexController);
    consultantIndexController.$inject = ['$scope', '$http', 'Pagination'];
    function consultantIndexController($scope, $http, Pagination) {
        //分页条
        $scope.pager = new Pagination({
            start: 0, limit: 10, total: 0, page: 1
        });
        $scope.pageSizeList = [10, 20, 50, 100];
        $scope.pageChange = function () {
            if ($scope.pager.page > $scope.pager.pages) {
                $scope.pager.page = $scope.pager.pages;
            }
            getConsultantList();
        };
        $scope.pageSizeChange = function () {
            getConsultantList();
        };

        //加载表单数据
        var getConsultantList = function () {
            var postPara = {
                'vendorName': $scope.VendorName,
                'name': $scope.Name,
                'pageIndex': $scope.pager.page,
                'pageSize': $scope.pager.limit
            };
            $http({ 'method': 'GET', url: '/Api/Vendor/GetConsultantList', params: postPara }).success(function (results) {
                if (results) {
                    $scope.pager.set({ total: results.total });
                    $scope.consultantList = results.list;
                }
            });
        };
        getConsultantList();
    }

    //Consultant-Edit
    angular.module('app').controller('consultantEditController', consultantEditController);
    consultantEditController.$inject = ['$scope', '$routeParams', '$http', 'Pagination', 'Upload', 'msgService', 'vendorSelectorService'];
    function consultantEditController($scope, $routeParams, $http, Pagination, Upload, msgService, vendorSelectorService) {
        $scope.Id = $routeParams.Id;
        $scope.submitted = false;

        //加载模型数据
        if ($scope.Id && $scope.Id != '') {
            $http({ 'url': '/Api/Vendor/GetConsultant', 'method': 'GET', 'params': { 'Id': $scope.Id } }).success(function (results) {
                if (results && results.isAccess) {
                    $scope.name = results.model.name;
                    $scope.address = results.model.address;
                    $scope.tel = results.model.tel;
                    $scope.email = results.model.email;
                    $scope.remark = results.model.remark;
                    $scope.vendor = results.model.vendor; 

                    if (results.attachfileList && results.attachfileList.length > 0) {
                        $scope.Attachfile = results.attachfileList;
                    }
                }
            });
        }

        //选择供应商
        $scope.btnSearchVendor_Click = function () {
            vendorSelectorService.singleSelector({
                'closeCallback': function (vendor) {
                    $scope.vendor = vendor;
                }
            });
        };

        //上传附件
        $scope.fileUploadAttachfile_Select = function ($file) {
            try {
                $scope.UploadProcess = '0%';
                $scope.IsShowProcessbar = true;
                Upload.upload({
                    url: '/Api/Vendor/UploadConsultantAttachfile',
                    data: {
                        file: $file
                    }
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.UploadProcess = progressPercentage + '%';
                }).success(function (data, status, headers, config) {
                    $scope.UploadAttachfile = '';
                    $scope.UploadProcess = '0%';
                    $scope.IsShowProcessbar = false;
                    $scope.Attachfile = $scope.Attachfile || [];
                    if (data && data.file)
                        $scope.Attachfile.push(data.file);
                });
            }
            catch (ex) {
                $scope.UploadAttachfile = '';
                UnMaskbody();
                console.log(ex);
            }
        };

        //删除附件
        $scope.btnDeleteAttachfile_Click = function ($index, file) {
            $scope.Attachfile.splice($index, 1);
        };

        //提交
        $scope.btnSubmit_Click = function () {
            if ($scope.consultant.$valid) {
                var attachfileIdStr = '';
                if ($scope.Attachfile && $scope.Attachfile.length > 0) {
                    var attachIdArr = [];
                    for (var i = 0; i < $scope.Attachfile.length; i++) {
                        attachIdArr.push($scope.Attachfile[i].id);
                    }
                    attachfileIdStr = attachIdArr.join(',');
                }
                var postParam = {
                    'Id': $scope.Id,
                    'Name': $scope.name,
                    'Address': $scope.address,
                    'Tel': $scope.tel,
                    'Email': $scope.email,
                    'Email': $scope.email,
                    'Remark': $scope.remark,
                    'VendorId': $scope.vendor.id,
                    'AttachfileId': attachfileIdStr
                };
                $http.post('/Api/Vendor/ConsultantEdit', postParam).success(function (result) {
                    if (!result.isAccess) {
                        msgService.alert({ 'message': '请检查表单！' });
                    }
                    else {
                        msgService.alert({
                            'message': '保存成功！', 'okButtonClick': function () {
                                window.location.href = '/#/Vendor/Consultant';
                            }
                        });
                    }
                });
            }
            else {
                $scope.consultant.submitted = true;
            }
        };
    }
})();
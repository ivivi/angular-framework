﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Identity;
using App.Core.Data;
using App.Core.Service.Identity;
using App.Core.Data.Repositories.Identity;

namespace App.Service.Service.Identity
{
    class RoleService : BaseService<RoleEntity>, IRoleService
    {
        readonly IRoleRepository iRoleRepository;
        public RoleService(IUnitOfWork unitOfWork,
            IRoleRepository roleRepository)
            : base(unitOfWork)
        {
            this.iRoleRepository = roleRepository;
        }
    }
}

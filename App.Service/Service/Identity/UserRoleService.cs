﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Identity;
using App.Core.Data;
using App.Core.Service.Identity;
using App.Core.Data.Repositories.Identity;
namespace App.Service.Service.Identity
{
    public class UserRoleService : BaseService<UserRoleEntity>, IUserRoleService
    {
        readonly IUserRoleRepository iUserRoleRepository;
        public UserRoleService(IUnitOfWork unitOfWork,
            IUserRoleRepository userRoleRepository)
            : base(unitOfWork)
        {
            this.iUserRoleRepository = userRoleRepository;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Identity;
using App.Core.Data;
using App.Core.Service.Identity;
using App.Core.Data.Repositories.Identity;

namespace App.Service.Service.Identity
{
    class ModuleService : BaseService<ModuleEntity>, IModuleService
    {
        readonly IModuleRepository iModuleRepository;
        public ModuleService(IUnitOfWork unitOfWork,
            IModuleRepository moduleRepository)
            : base(unitOfWork)
        {
            this.iModuleRepository = moduleRepository;
        }
    }
}

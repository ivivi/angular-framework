﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Identity;
using App.Core.Data; 
using App.Core.Service.Identity;
using App.Core.Data.Repositories.Identity;

namespace App.Service.Service.Identity
{
    public class MenuService:BaseService<MenuEntity>, IMenuService
    {
        readonly IMenuRepository iMenuRepository;
        public MenuService(IUnitOfWork unitOfWork,
            IMenuRepository menuRepository)
            : base(unitOfWork)
        {
            this.iMenuRepository = menuRepository;
        } 
    }
}

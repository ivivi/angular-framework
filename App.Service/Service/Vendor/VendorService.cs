﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Data;
using App.Core.Entities.Vendor;
using App.Core.Service.Vendor;
using App.Core.Data.Repositories.Vendor;

namespace App.Service.Service.Vendor
{
    public class VendorService:BaseService<VendorEntity>,IVendorService
    {
        IVendorRepository vendorRepository;
        IVendorAttachfileRepository vendorAttachfileRepository;

        public VendorService(IUnitOfWork unitOfWork,
            IVendorRepository vendorRepository,
            IVendorAttachfileRepository vendorAttachfileRepository)
            : base(unitOfWork)
        {
            this.vendorRepository = vendorRepository;
            this.vendorAttachfileRepository = vendorAttachfileRepository;
        }

        public VendorEntity Create(VendorEntity entity, string attachfileId)
        {
            this.vendorRepository.Add(entity);
            if (!String.IsNullOrEmpty(attachfileId))
            {
                int[] attachfileIdArr = attachfileId.Split(',').Where(x => !String.IsNullOrEmpty(x)).Select(x => Int32.Parse(x)).ToArray();
                var attachfileEntities = this.vendorAttachfileRepository.GetAll(x => attachfileIdArr.Contains(x.Id) && !x.VendorId.HasValue);
                foreach (var attachfileEntity in attachfileEntities)
                {
                    attachfileEntity.Vendor = entity;
                    this.vendorAttachfileRepository.Update(attachfileEntity);
                }
            }
            this.unitOfWork.Commit();
            return entity;
        }

        public void Edit(VendorEntity entity, string attachfileId)
        {
            IList<VendorAttachfileEntity> attachfileEntitieExists = new List<VendorAttachfileEntity>();
            if (entity.Attachfiles != null && entity.Attachfiles.Count > 0)
            {
                attachfileEntitieExists = entity.Attachfiles.ToList();
            }

            if (!String.IsNullOrEmpty(attachfileId))
            {
                int[] attachfileIdArr = attachfileId.Split(',').Where(x => !String.IsNullOrEmpty(x)).Select(x => Int32.Parse(x)).ToArray();
                var attachfileEntities = this.vendorAttachfileRepository.GetAll(x => attachfileIdArr.Contains(x.Id) && !x.VendorId.HasValue);

                foreach (var item in attachfileEntities)
                {
                    item.Vendor = entity;
                    item.VendorId = entity.Id;
                    this.vendorAttachfileRepository.Update(item);
                }

                foreach (var item in attachfileEntitieExists)
                {
                    if (!attachfileIdArr.Contains(item.Id))
                    {
                        this.vendorAttachfileRepository.Delete(item);
                    }
                }
            }
            else
            {
                foreach (var item in attachfileEntitieExists)
                {
                    this.vendorAttachfileRepository.Delete(item);
                }
            }
            this.vendorRepository.Update(entity);
            this.unitOfWork.Commit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Data;
using App.Core.Entities.Vendor;
using App.Core.Service.Vendor;
using App.Core.Data.Repositories.Vendor;

namespace App.Service.Service.Vendor
{
    public class VendorAttachfileService:BaseService<VendorAttachfileEntity>,IVendorAttachfileService
    {
        IVendorAttachfileRepository vendorAttachfileRepository;
        public VendorAttachfileService(IUnitOfWork unitOfWork,
            IVendorAttachfileRepository vendorAttachfileRepository)
            : base(unitOfWork)
        {
            this.vendorAttachfileRepository = vendorAttachfileRepository;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Data;
using App.Core.Data.Repositories.Vendor;
using App.Core.Entities.Vendor;
using App.Core.Service.Vendor;

namespace App.Service.Service.Vendor
{
    public class ConsultantAttachfileService:BaseService<ConsultantAttachfileEntity>,IConsultantAttachfileService
    {
        IConsultantAttachfileRepository consultantAttachfileRepository;
        public ConsultantAttachfileService(IUnitOfWork unitOfWork,
            IConsultantAttachfileRepository consultantAttachfileRepository)
            : base(unitOfWork)
        {
            this.consultantAttachfileRepository = consultantAttachfileRepository;
        }
    }
}

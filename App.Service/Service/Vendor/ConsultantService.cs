﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Data;
using App.Core.Data.Repositories.Vendor;
using App.Core.Entities.Vendor;
using App.Core.Service.Vendor;

namespace App.Service.Service.Vendor
{
    public class ConsultantService:BaseService<ConsultantEntity>,IConsultantService
    {
        IConsultantRepository consultantRepository;
        IConsultantAttachfileRepository consultantAttachfileRepository;
        public ConsultantService(IUnitOfWork unitOfWork,
            IConsultantRepository consultantRepository,
            IConsultantAttachfileRepository consultantAttachfileRepository)
            : base(unitOfWork)
        {
            this.consultantRepository = consultantRepository;
            this.consultantAttachfileRepository = consultantAttachfileRepository;
        }

        public ConsultantEntity Create(ConsultantEntity entity, string attachfileId)
        {
            this.consultantRepository.Add(entity);
            if (!String.IsNullOrEmpty(attachfileId))
            {
                int[] attachfileIdArr = attachfileId.Split(',').Where(x => !String.IsNullOrEmpty(x)).Select(x => Int32.Parse(x)).ToArray();
                var attachfileEntities = this.consultantAttachfileRepository.GetAll(x => attachfileIdArr.Contains(x.Id) && !x.ConsultantId.HasValue);
                foreach (var attachfileEntity in attachfileEntities)
                {
                    attachfileEntity.Consultant = entity;
                    this.consultantAttachfileRepository.Update(attachfileEntity);
                }
            }
            this.unitOfWork.Commit();
            return entity;
        }

        public void Edit(ConsultantEntity entity, string attachfileId)
        {
            IList<ConsultantAttachfileEntity> attachfileEntitieExists = new List<ConsultantAttachfileEntity>();
            if (entity.Attachfiles != null && entity.Attachfiles.Count > 0)
            {
                attachfileEntitieExists = entity.Attachfiles.ToList();
            }

            if (!String.IsNullOrEmpty(attachfileId))
            {
                int[] attachfileIdArr = attachfileId.Split(',').Where(x => !String.IsNullOrEmpty(x)).Select(x => Int32.Parse(x)).ToArray();
                var attachfileEntities = this.consultantAttachfileRepository.GetAll(x => attachfileIdArr.Contains(x.Id) && !x.ConsultantId.HasValue);

                foreach (var item in attachfileEntities)
                {
                    item.Consultant = entity;
                    item.ConsultantId = entity.Id;
                    this.consultantAttachfileRepository.Update(item);
                }

                foreach (var item in attachfileEntitieExists)
                {
                    if (!attachfileIdArr.Contains(item.Id))
                    {
                        this.consultantAttachfileRepository.Delete(item);
                    }
                }
            }
            else
            {
                foreach (var item in attachfileEntitieExists)
                {
                    this.consultantAttachfileRepository.Delete(item);
                }
            }
            this.consultantRepository.Update(entity);
            this.unitOfWork.Commit();
        }
    }
}

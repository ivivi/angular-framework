﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Dto.Dtos
{
    /// <summary>
    /// 绑定到Class上，允许绑定多次
    /// </summary>
    [AttributeUsage(AttributeTargets.Class,AllowMultiple = true,Inherited = false )]
    public  class ToEntityAttribute:Attribute
    {
        /// <summary>
        /// 多个实体名称之间，可以用逗号分隔
        /// </summary>
        public string EntityName { get; set; }
        public ToEntityAttribute(string entityName) {
            this.EntityName = entityName;
        } 
    }
}

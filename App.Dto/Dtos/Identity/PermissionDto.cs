﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Dto.Dtos.Identity
{
    public class PermissionDto
    {
        public string Permission { get; set; }
        public bool IsSelected { get; set; }
    }
}

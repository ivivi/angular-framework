﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Dto.Dtos;

namespace App.Dto.Dtos.Identity
{
    public class MenuDto:BaseDto
    { 
        /// <summary>
        /// 菜单标题
        /// </summary>
        public string Title { get; set; } 

        public string Icon { get; set; }
        public string URL { get; set; }

        public int ParentId { get; set; }

        public string View { get; set; }

        public int SortNo { get; set; }
    }
}

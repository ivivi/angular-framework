﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Dto.Dtos.Identity
{
    public class RoleDto:BaseDto
    {
        public string Name { get; set; }

        public string ModulePermissions{get;set;}
    }
}

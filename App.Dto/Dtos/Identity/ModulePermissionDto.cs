﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Dto.Dtos.Identity
{
    public class ModulePermissionDto
    {
        /// <summary>
        /// 模块
        /// </summary>
        public string Module { get; set; }

        /// <summary>
        /// 功能权限：[{View:true},{Create:false}]
        /// </summary>
        public IList<PermissionDto> Permissions { get; set; }
    }
}

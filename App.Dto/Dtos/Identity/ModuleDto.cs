﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Dto.Dtos.Identity
{
    public class ModuleDto:BaseDto
    {
        public string Module { get; set; }

        /// <summary>
        /// 权限集合，用逗号分隔
        /// </summary>
        public string Permissions { get; set; }
    }
}

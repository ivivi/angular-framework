﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Dto.Dtos;

namespace App.Dto.Dtos.Identity
{
    [ToEntity("MenuEntity")]
    [ToEntity("UserEntity")]
    [ToEntity("UserEntity,LogEntity")]
    public class TestDto
    {
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatedBy { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdatedDate { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public string UpdatedBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Dto.Dtos.Vendor
{
    public class ConsultantDto
    { 
        public int Id { get; set; } 
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        /// <summary>
        /// 顾问姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 城市/地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string Tel { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 公司编号
        /// </summary>
        public int? VendorId { get; set; }

        public VendorDto Vendor { get; set; }

        public string AttachfileId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using System.ComponentModel.DataAnnotations;

namespace App.Dto.Dtos.Vendor
{
    public class VendorDto
    {
        public int Id { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary> 
        [Required]
        public string CompanyName { get; set; }

        /// <summary>
        /// 公司地址
        /// </summary>
        [Required]
        public string CompanyAddress { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        [Required]
        public string ContactPersion { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        [Required]
        public string Tel { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        [Required]
        public string PostCode { get; set; }

        /// <summary>
        /// 电子邮箱
        /// </summary>
        [Required, EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// 公司缩写
        /// </summary>
        [Required]
        public string Combination { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [MaxLength(200)]
        public string Remark { get; set; }

        public string AttachfileId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Dto.Dtos.Vendor
{
    public class ConsultantAttachfileDto
    {
        public int Id { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string FileName { get; set; }
        public string FileSize { get; set; }
    }
}

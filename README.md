#APP.NET
APP.NET 是一个基于.NET开发环境的后台管理系统框架
系统使用技术模块：
1. Asp.NET MVC
1. Asp.NET WebApi
1. Asp.NET Identity
1. Autofac
1. AutoMapper
1. NLog
1. Angularjs
1. Bootstarp 
1. Entity Framework
1. Redis

### 开发环境
1. ASP.NET4.5 （平台）
1. Visul Studio 2012+(建议安装新版的Nuget)
1. SQL Server2005+ 
1. Redis (缓存)

### 配置信息
1. 创建数据库并数据库连接字符串：AppDbConnection
1. 使用Nuget命令创建数据库表结构：Update-Database
1. 添加默认数据，运行文件 初始化数据.sql 
1. 缓存默认使用Redis（ServiceStack.Redis组建使用3.9版本）
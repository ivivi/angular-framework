﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using App.Logging.Logging;
using App.Core.Logging;

namespace App.Logging.Logging
{
    public  class NLogLogger:ILogger
    {
        private static readonly Lazy<NLogLogger> lazyLogger = new Lazy<NLogLogger>(() => new NLogLogger());
        private static readonly Lazy<NLog.Logger> lazyNLogger = new Lazy<NLog.Logger>(NLog.LogManager.GetCurrentClassLogger);

        public static ILogger Instance
        {
            get{ return lazyLogger.Value; }
        }
        public NLogLogger() { }

        public void Log(string message)
        {
            lazyNLogger.Value.Info(message);
        }
        public void Log(Exception ex)
        {
            lazyNLogger.Value.Error(ex);
        }
    }
}

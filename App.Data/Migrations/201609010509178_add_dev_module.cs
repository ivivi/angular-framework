namespace App.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_dev_module : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dev_BugAssign",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssignTo = c.String(maxLength: 200),
                        BugId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dev_Bug", t => t.BugId)
                .Index(t => t.BugId);
            
            CreateTable(
                "dbo.Dev_Bug",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 200),
                        Assigner = c.String(maxLength: 200),
                        Status = c.String(maxLength: 200),
                        SourtNo = c.Int(nullable: false),
                        IsHighline = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Dev_BugAttachfile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BugId = c.Int(),
                        FileName = c.String(maxLength: 2000),
                        FileSavedPath = c.String(maxLength: 2000),
                        FileSize = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dev_Bug", t => t.BugId)
                .Index(t => t.BugId);
            
            CreateTable(
                "dbo.Dev_BugHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BugId = c.Int(),
                        Action = c.String(maxLength: 200),
                        Status = c.String(maxLength: 200),
                        Remark = c.String(maxLength: 2000),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dev_Bug", t => t.BugId)
                .Index(t => t.BugId);
            
            CreateTable(
                "dbo.Dev_Publish",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SourceCodeKey = c.String(maxLength: 200),
                        Description = c.String(maxLength: 2000),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Dev_BugAssign", "BugId", "dbo.Dev_Bug");
            DropForeignKey("dbo.Dev_BugHistory", "BugId", "dbo.Dev_Bug");
            DropForeignKey("dbo.Dev_BugAttachfile", "BugId", "dbo.Dev_Bug");
            DropIndex("dbo.Dev_BugHistory", new[] { "BugId" });
            DropIndex("dbo.Dev_BugAttachfile", new[] { "BugId" });
            DropIndex("dbo.Dev_BugAssign", new[] { "BugId" });
            DropTable("dbo.Dev_Publish");
            DropTable("dbo.Dev_BugHistory");
            DropTable("dbo.Dev_BugAttachfile");
            DropTable("dbo.Dev_Bug");
            DropTable("dbo.Dev_BugAssign");
        }
    }
}

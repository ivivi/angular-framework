namespace App.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_table_vendor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Vendor_ConsultantAttachfile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConsultantId = c.Int(),
                        FileName = c.String(),
                        FileSavedPath = c.String(),
                        FileSize = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vendor_Consultant", t => t.ConsultantId)
                .Index(t => t.ConsultantId);
            
            CreateTable(
                "dbo.Vendor_Consultant",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                        Address = c.String(maxLength: 200),
                        Tel = c.String(maxLength: 50),
                        Email = c.String(maxLength: 50),
                        VendorId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vendor_Vendor", t => t.VendorId)
                .Index(t => t.VendorId);
            
            CreateTable(
                "dbo.Vendor_Vendor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(maxLength: 200),
                        CompanyAddress = c.String(maxLength: 200),
                        ContactPersion = c.String(maxLength: 200),
                        Tel = c.String(maxLength: 50),
                        PostCode = c.String(maxLength: 50),
                        Email = c.String(maxLength: 50),
                        Combination = c.String(maxLength: 50),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vendor_VendorAttachfile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VendorId = c.Int(),
                        FileName = c.String(),
                        FileSavedPath = c.String(),
                        FileSize = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vendor_Vendor", t => t.VendorId)
                .Index(t => t.VendorId);
            
            AddColumn("dbo.Sys_Menu", "URL", c => c.String(maxLength: 400));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Vendor_ConsultantAttachfile", "ConsultantId", "dbo.Vendor_Consultant");
            DropForeignKey("dbo.Vendor_Consultant", "VendorId", "dbo.Vendor_Vendor");
            DropForeignKey("dbo.Vendor_VendorAttachfile", "VendorId", "dbo.Vendor_Vendor");
            DropIndex("dbo.Vendor_VendorAttachfile", new[] { "VendorId" });
            DropIndex("dbo.Vendor_Consultant", new[] { "VendorId" });
            DropIndex("dbo.Vendor_ConsultantAttachfile", new[] { "ConsultantId" });
            DropColumn("dbo.Sys_Menu", "URL");
            DropTable("dbo.Vendor_VendorAttachfile");
            DropTable("dbo.Vendor_Vendor");
            DropTable("dbo.Vendor_Consultant");
            DropTable("dbo.Vendor_ConsultantAttachfile");
        }
    }
}

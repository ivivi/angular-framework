namespace App.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_log : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sys_Log",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Application = c.String(maxLength: 50),
                        Level = c.String(maxLength: 50),
                        Message = c.String(storeType: "ntext"),
                        ServerName = c.String(),
                        Port = c.String(),
                        Url = c.String(),
                        Https = c.Boolean(nullable: false),
                        ServerAddress = c.String(maxLength: 100),
                        RemoteAddress = c.String(maxLength: 100),
                        Logger = c.String(maxLength: 200),
                        Callsite = c.String(),
                        Exception = c.String(storeType: "ntext"),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Sys_Log");
        }
    }
}

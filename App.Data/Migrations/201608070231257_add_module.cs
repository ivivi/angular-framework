namespace App.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_module : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sys_Module",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Module = c.String(maxLength: 500),
                        Permissions = c.String(maxLength: 2000),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Sys_Menu", "View", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sys_Menu", "View");
            DropTable("dbo.Sys_Module");
        }
    }
}

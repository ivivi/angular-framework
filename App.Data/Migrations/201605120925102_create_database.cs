namespace App.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_database : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sys_Menu",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 200),
                        Icon = c.String(maxLength: 200),
                        ParentId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sys_Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sys_UserRole",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                        RoleEntity_Id = c.Int(),
                        UserEntity_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Sys_Role", t => t.RoleEntity_Id)
                .ForeignKey("dbo.Sys_User", t => t.UserEntity_Id)
                .Index(t => t.RoleEntity_Id)
                .Index(t => t.UserEntity_Id);
            
            CreateTable(
                "dbo.Sys_User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(maxLength: 200),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(maxLength: 4000),
                        SecurityStamp = c.String(maxLength: 4000),
                        PhoneNumber = c.String(maxLength: 50),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sys_UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        UserEntity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sys_User", t => t.UserEntity_Id)
                .Index(t => t.UserEntity_Id);
            
            CreateTable(
                "dbo.Sys_UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                        UserEntity_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Sys_User", t => t.UserEntity_Id)
                .Index(t => t.UserEntity_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sys_UserRole", "UserEntity_Id", "dbo.Sys_User");
            DropForeignKey("dbo.Sys_UserLogin", "UserEntity_Id", "dbo.Sys_User");
            DropForeignKey("dbo.Sys_UserClaim", "UserEntity_Id", "dbo.Sys_User");
            DropForeignKey("dbo.Sys_UserRole", "RoleEntity_Id", "dbo.Sys_Role");
            DropIndex("dbo.Sys_UserLogin", new[] { "UserEntity_Id" });
            DropIndex("dbo.Sys_UserClaim", new[] { "UserEntity_Id" });
            DropIndex("dbo.Sys_UserRole", new[] { "UserEntity_Id" });
            DropIndex("dbo.Sys_UserRole", new[] { "RoleEntity_Id" });
            DropTable("dbo.Sys_UserLogin");
            DropTable("dbo.Sys_UserClaim");
            DropTable("dbo.Sys_User");
            DropTable("dbo.Sys_UserRole");
            DropTable("dbo.Sys_Role");
            DropTable("dbo.Sys_Menu");
        }
    }
}

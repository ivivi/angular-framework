namespace App.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_vendor_remark : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vendor_Consultant", "Remark", c => c.String());
            AddColumn("dbo.Vendor_Vendor", "Remark", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Vendor_Vendor", "Remark");
            DropColumn("dbo.Vendor_Consultant", "Remark");
        }
    }
}

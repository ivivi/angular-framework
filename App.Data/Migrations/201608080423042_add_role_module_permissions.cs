namespace App.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_role_module_permissions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sys_Role", "ModulePermissions", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sys_Role", "ModulePermissions");
        }
    }
}

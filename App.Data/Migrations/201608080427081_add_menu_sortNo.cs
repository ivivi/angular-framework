namespace App.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_menu_sortNo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sys_Menu", "SortNo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sys_Menu", "SortNo");
        }
    }
}

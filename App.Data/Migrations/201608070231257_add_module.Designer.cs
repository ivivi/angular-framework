// <auto-generated />
namespace App.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class add_module : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(add_module));
        
        string IMigrationMetadata.Id
        {
            get { return "201608070231257_add_module"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

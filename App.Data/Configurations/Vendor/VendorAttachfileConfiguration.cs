﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Vendor;

namespace App.Data.Configurations.Vendor
{
    public class VendorAttachfileConfiguration : BaseConfiguration<VendorAttachfileEntity>
    {
        public VendorAttachfileConfiguration()
        {
            this.ToTable("Vendor_VendorAttachfile");
            this.HasOptional(x => x.Vendor).WithMany(x => x.Attachfiles).HasForeignKey(x => x.VendorId);
        }
    }
}

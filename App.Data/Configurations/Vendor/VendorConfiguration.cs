﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Core.Entities.Vendor;

namespace App.Data.Configurations.Vendor
{
    public class VendorConfiguration : BaseConfiguration<VendorEntity>
    {
        public VendorConfiguration()
        {
            this.ToTable("Vendor_Vendor"); 
            Property(x => x.CompanyName).HasMaxLength(200);
            Property(x => x.CompanyAddress).HasMaxLength(200);
            Property(x => x.ContactPersion).HasMaxLength(200);
            Property(x => x.Tel).HasMaxLength(50);
            Property(x => x.PostCode).HasMaxLength(50);
            Property(x => x.Email).HasMaxLength(50);
            Property(x => x.Combination).HasMaxLength(50); 
        }
    }
}

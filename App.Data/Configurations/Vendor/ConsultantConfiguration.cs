﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Vendor;

namespace App.Data.Configurations.Vendor
{
    public class ConsultantConfiguration:BaseConfiguration<ConsultantEntity>
    {
        public ConsultantConfiguration() {
            this.ToTable("Vendor_Consultant");
            Property(x => x.Name).HasMaxLength(200);
            Property(x => x.Address).HasMaxLength(200); 
            Property(x => x.Tel).HasMaxLength(50); 
            Property(x => x.Email).HasMaxLength(50);
            this.HasOptional(x => x.Vendor).WithMany(x => x.Consultants).HasForeignKey(x => x.VendorId);
        }
        
    }
}

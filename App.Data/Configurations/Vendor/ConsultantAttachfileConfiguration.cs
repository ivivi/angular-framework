﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Vendor;

namespace App.Data.Configurations.Vendor
{
    public class ConsultantAttachfileConfiguration:BaseConfiguration<ConsultantAttachfileEntity>
    {
        public ConsultantAttachfileConfiguration()
        { 
            this.ToTable("Vendor_ConsultantAttachfile");
            this.HasOptional(x => x.Consultant).WithMany(x => x.Attachfiles).HasForeignKey(x => x.ConsultantId);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Dev;

namespace App.Data.Configurations.Dev
{
    public class BugConfiguration : BaseConfiguration<BugEntity>
    {
        public BugConfiguration()
        {
            this.ToTable("Dev_Bug");
            Property(c => c.Description).HasMaxLength(200);
            Property(c => c.Assigner).HasMaxLength(200);
            Property(c => c.Status).HasMaxLength(200);
        }
    }
}

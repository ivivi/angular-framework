﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Dev;

namespace App.Data.Configurations.Dev
{
    public class BugAssignConfiguration : BaseConfiguration<BugAssignEntity>
    {
        public BugAssignConfiguration()
        {
            this.ToTable("Dev_BugAssign");
            Property(c => c.AssignTo).HasMaxLength(200);
            this.HasOptional(x => x.Bug).WithMany(x => x.BugAssignEntities).HasForeignKey(x => x.BugId);
        }
    }
}

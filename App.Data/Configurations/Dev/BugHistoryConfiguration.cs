﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Dev;

namespace App.Data.Configurations.Dev
{
    public class BugHistoryConfiguration : BaseConfiguration<BugHistoryEntity>
    {
        public BugHistoryConfiguration()
        {
            this.ToTable("Dev_BugHistory");
            Property(c => c.Action).HasMaxLength(200);
            Property(c => c.Status).HasMaxLength(200);
            Property(c => c.Remark).HasMaxLength(2000);
            this.HasOptional(x => x.Bug).WithMany(x => x.Histories).HasForeignKey(x => x.BugId);
        }
    }
}

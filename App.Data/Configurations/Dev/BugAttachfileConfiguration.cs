﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Dev;

namespace App.Data.Configurations.Dev
{
    public class BugAttachfileConfiguration : BaseConfiguration<BugAttachfileEntity>
    {
        public BugAttachfileConfiguration()
        {
            this.ToTable("Dev_BugAttachfile");
            Property(c => c.FileName).HasMaxLength(2000);
            Property(c => c.FileSavedPath).HasMaxLength(2000);
            Property(c => c.FileSize).HasMaxLength(200);

            this.HasOptional(x => x.Bug).WithMany(x => x.Attachfiles).HasForeignKey(x => x.BugId);
        }
    }
}

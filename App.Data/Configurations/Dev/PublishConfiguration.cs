﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Dev;

namespace App.Data.Configurations.Dev
{
    public class PublishConfiguration : BaseConfiguration<PublishEntity>
    {
        public PublishConfiguration()
        {
            this.ToTable("Dev_Publish");
            Property(x => x.SourceCodeKey).HasMaxLength(200);
            Property(x => x.Description).HasMaxLength(2000);
        }
    }
}

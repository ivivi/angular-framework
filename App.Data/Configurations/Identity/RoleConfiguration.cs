﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Core.Entities.Identity;

namespace App.Data.Configurations.Identity
{
    class RoleConfiguration: EntityTypeConfiguration<RoleEntity>
    {
        public RoleConfiguration()
        {
            this.ToTable("Sys_Role");
            Property(x => x.Name).HasMaxLength(200);
            Property(x => x.ModulePermissions).HasMaxLength(4000);
        }
    }
}

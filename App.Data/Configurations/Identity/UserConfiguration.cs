﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Core.Entities.Identity;

namespace App.Data.Configurations.Identity
{
    public class UserConfiguration:EntityTypeConfiguration<UserEntity>
    {
        public UserConfiguration()
        {
            this.ToTable("Sys_User");
            Property(c => c.UserName).HasMaxLength(200);
            Property(c => c.Email).HasMaxLength(200);
            Property(x => x.PasswordHash).HasMaxLength(4000);
            Property(x => x.SecurityStamp).HasMaxLength(4000);
            Property(x => x.PhoneNumber).HasMaxLength(50);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Identity;

namespace App.Data.Configurations.Identity
{
    public class LogConfiguration : BaseConfiguration<LogEntity>
    {
        public LogConfiguration()
        {
            this.ToTable("Sys_Log");
            Property(x => x.Application).HasMaxLength(50);
            Property(x => x.Level).HasMaxLength(50);
            Property(x => x.Message).HasColumnType("NText");
            Property(x => x.ServerName).HasColumnType("Nvarchar(max)");
            Property(x => x.Port).HasColumnType("Nvarchar(max)");
            Property(x => x.Url).HasColumnType("Nvarchar(max)");
            Property(x => x.ServerAddress).HasMaxLength(100);
            Property(x => x.RemoteAddress).HasMaxLength(100);
            Property(x => x.Logger).HasMaxLength(200);
            Property(x => x.Callsite).HasColumnType("Nvarchar(max)");
            Property(x => x.Exception).HasColumnType("NText");
        }
    }
}

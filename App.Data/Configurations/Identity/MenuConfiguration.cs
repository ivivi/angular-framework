﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Core.Entities.Identity;


namespace App.Data.Configurations.Identity
{
    public class MenuConfiguration: BaseConfiguration<MenuEntity>
    {
        public MenuConfiguration()
        {
            this.ToTable("Sys_Menu"); 
            Property(x => x.Icon).HasMaxLength(200); 
            Property(x => x.Title).HasMaxLength(200);
            Property(x => x.URL).HasMaxLength(400);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Core.Entities.Identity;


namespace App.Data.Configurations.Identity
{
    public class ModuleConfiguration :BaseConfiguration<ModuleEntity>
    {
        public ModuleConfiguration()
        {
            this.ToTable("Sys_Module");
            Property(x => x.Module).HasMaxLength(500);
            Property(x => x.Permissions).HasMaxLength(2000);
        }
    }
}

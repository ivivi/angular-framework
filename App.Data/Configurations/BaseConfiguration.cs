﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using App.Core.Entities.Foundation;

namespace App.Data.Configurations
{
    public class BaseConfiguration<TEntity>: EntityTypeConfiguration<TEntity> where TEntity:BaseEntity
    {
        public BaseConfiguration()
        {
            this.HasKey(x => x.Id);
            Property(x => x.CreatedBy).HasMaxLength(50);
            Property(x => x.UpdatedBy).HasMaxLength(50);
        }
    }
}

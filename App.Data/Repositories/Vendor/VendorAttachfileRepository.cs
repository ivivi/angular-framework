﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Data.Repositories.Vendor;
using App.Core.Entities.Vendor;

namespace App.Data.Repositories.Vendor
{
    public class VendorAttachfileRepository:BaseRepository<VendorAttachfileEntity>,IVendorAttachfileRepository
    {
        public VendorAttachfileRepository(IDbContext context) : base(context)
        {

        }
    }
}

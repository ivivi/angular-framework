﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Vendor;
using App.Core.Data.Repositories.Vendor;

namespace App.Data.Repositories.Vendor
{
    public class VendorRepository:BaseRepository<VendorEntity>,IVendorRepository
    {
        public VendorRepository(IDbContext context) : base (context)
        {

        }
    }
}

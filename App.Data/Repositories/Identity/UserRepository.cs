﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Identity;
using App.Core.Data.Repositories.Identity;

namespace App.Data.Repositories.Identity
{
    public class UserRepository : BaseRepository<UserEntity>, IUserRepository
    {
        public UserRepository(IDbContext context) : base (context)
        {
        }
    }
}

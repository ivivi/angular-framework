﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Identity;
using App.Core.Data.Repositories.Identity;


namespace App.Data.Repositories.Identity
{
    public class RoleRepository : BaseRepository<RoleEntity>, IRoleRepository
    {
        public RoleRepository(IDbContext context) : base (context)
        {
        }
    }
}

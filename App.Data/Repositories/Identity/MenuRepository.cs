﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Identity; 
using App.Core.Data.Repositories.Identity;

namespace App.Data.Repositories
{
    public class MenuRepository: BaseRepository<MenuEntity>, IMenuRepository
    {
        public MenuRepository(IDbContext context) : base (context)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using App.Core.Entities.Foundation;

namespace App.Data
{
    public interface IDbContext:IDisposable
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;

        void SetAsAdded<TEntity>(TEntity entity) where TEntity : class;

        void SetAsModified<TEntity>(TEntity entity) where TEntity : class;

        void SetAsDeleted<TEntity>(TEntity entity) where TEntity : class;

        void BeginTransaction();

        int Commit();
        Task<int> CommitAsync();

        void Rollback();
    }
}

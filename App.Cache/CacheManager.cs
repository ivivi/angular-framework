﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Cache;
using App.Core.Infrastructure;
using ServiceStack.Redis;

namespace App.Cache
{
    public class CacheManager : ICacheManager
    {
        readonly IRedisClient client;
        public CacheManager()
        { 
            string redisServer = Config.GetAppSettings("RedisServer");
            string redisServerPort = Config.GetAppSettings("RedisServerPort");
            this.client = new RedisClient(redisServer,Int32.Parse(redisServerPort));
        }

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">key</param>
        /// <returns></returns>
        public T Get<T>(string key) where T : class
        {
            return this.client.Get<T>(key);
        }

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            this.client.Remove(key);
        }

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        /// <param name="minutes">过期时间（单位：分钟，默认：20）</param>
        public void Set<T>(string key, T value, int minutes = 20) where T : class
        {
            TimeSpan timeSpan = TimeSpan.FromMinutes(20);
            this.client.Set<T>(key,value, timeSpan);
        }
    }
}

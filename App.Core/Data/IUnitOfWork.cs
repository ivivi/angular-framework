﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Data;
using App.Core.Data.Repositories;
using App.Core.Entities;
using App.Core.Entities.Foundation;

namespace App.Core.Data
{
    public interface IUnitOfWork:IDisposable
    {
        IRepository<TEntity> Repository<TEntity>() where TEntity : class;

        void BeginTransaction();

        int Commit();

        Task<int> CommitAsync();

        void Rollback();

        void Dispose(bool disposing);
    }
}

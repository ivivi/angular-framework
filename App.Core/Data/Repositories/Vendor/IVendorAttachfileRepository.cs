﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Vendor;

namespace App.Core.Data.Repositories.Vendor
{
    public interface IVendorAttachfileRepository : IRepository<VendorAttachfileEntity>
    {
    }
}

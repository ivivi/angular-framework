﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities;
using App.Core.Entities.Foundation;

namespace App.Core.Data.Repositories
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        IList<TEntity> GetAll();
        Task<List<TEntity>> GetAllAsync();

        IList<TEntity> GetAll(Expression<Func<TEntity,bool>> predicate);
        Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate);

        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);

        IList<TEntity> GetByPager<TKey>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TKey>> orderBy,int pageIndex,int pageSize,bool isOrderByAsc = true) where TKey : struct;
        Task<List<TEntity>> GetByPagerAsync<TKey>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TKey>> orderBy, int pageIndex, int pageSize, bool isOrderByAsc = true) where TKey : struct;

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);
        int Count();
        Task<int> CountAsync();

        int Count(Expression<Func<TEntity, bool>> predicate);
        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate);
    }
}

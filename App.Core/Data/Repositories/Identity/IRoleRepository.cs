﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Identity;

namespace App.Core.Data.Repositories.Identity
{
    public interface IRoleRepository : IRepository<RoleEntity>
    {
    }
}

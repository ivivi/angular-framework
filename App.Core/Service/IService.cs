﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using App.Core.Entities;
using App.Core.Entities.Foundation;

namespace App.Core.Service
{
    public interface IService<TEntity>:IDisposable where TEntity:class
    {
        IList<TEntity> GetAll();
        Task<List<TEntity>> GetAllAsync();

        IList<TEntity> GetAll(Expression<Func<TEntity,bool>> predicate);
        Task<List<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate);

        IList<TEntity> GetByPager<TKey>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TKey>> orderBy, int pageIndex, int pageSize, bool isOrderByAsc = true) where TKey : struct;
        Task<List<TEntity>> GetByPagerAsync<TKey>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TKey>> orderBy, int pageIndex, int pageSize, bool isOrderByAsc = true) where TKey : struct;


        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);

        TEntity Add(TEntity entity);

        Task<TEntity> AddAsync(TEntity entity);

        void Update(TEntity entity);

        Task UpdateAsync(TEntity entity);

        void Delete(TEntity entity);

        Task DeleteAsync(TEntity entity);

        Task<int> CountAsync();
        int Count();

        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate);
        int Count(Expression<Func<TEntity, bool>> predicate);
    }
}

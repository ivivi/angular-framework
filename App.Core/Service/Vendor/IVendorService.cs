﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Vendor;

namespace App.Core.Service.Vendor
{
    public interface IVendorService: IService<VendorEntity>
    {
        VendorEntity Create(VendorEntity entity,string attachfileId);
        void Edit(VendorEntity entity, string attachfileId);
    }
}

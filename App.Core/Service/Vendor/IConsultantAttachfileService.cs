﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Vendor;

namespace App.Core.Service.Vendor
{
    public interface IConsultantAttachfileService:IService<ConsultantAttachfileEntity>
    {
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Vendor;

namespace App.Core.Service.Vendor
{
    public interface IConsultantService:IService<ConsultantEntity>
    {
        ConsultantEntity Create(ConsultantEntity entity, string attachfileId);
        void Edit(ConsultantEntity entity, string attachfileId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Identity;

namespace App.Core.Cache
{
    /// <summary>
    /// 用户缓存数据（角色，权限。。。）
    /// </summary>
    public interface IUserCacheManager
    {
        /// <summary>
        /// 验证模块是否已经被授权
        /// </summary>
        /// <param name="module">模块名称</param>
        /// <param name="auth">授权名称</param>
        /// <returns></returns>
        bool IsAuth(string module,string auth);

        /// <summary>
        /// 获取当前用户
        /// </summary>
        /// <returns></returns>
        UserEntity GetUser();

        /// <summary>
        /// 获取权限列表
        /// </summary>
        /// <param name="module">模块名称</param>
        /// <returns></returns>
        IList<string> GetAuths(string module);

        /// <summary>
        /// 获取当前用户的角色列表
        /// </summary>
        /// <returns></returns>
        IList<RoleEntity> GetRoles();

        /// <summary>
        /// 获取当前用户的所有权限列表
        /// </summary>
        /// <returns></returns>
        IDictionary<string, string> GetAllModuleAuths();

        /// <summary>
        /// 刷新当前用户数据
        /// </summary>
        void Reset();
    }
}

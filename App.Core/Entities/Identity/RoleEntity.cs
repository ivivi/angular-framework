﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;

namespace App.Core.Entities.Identity
{
    [Serializable]
    public class RoleEntity:IdentityRole<int, UserRoleEntity>
    {
        /// <summary>
        /// 权限（如：Mudole1=功能1,功能2;Module2=功能1,功能2）
        /// </summary>
        public string ModulePermissions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using App.Core.Entities.Foundation; 

namespace App.Core.Entities.Identity
{ 
    [Serializable]
    public class UserEntity: IdentityUser<int, UserLoginEntity, UserRoleEntity, UserClaimEntity>
    {  
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<UserEntity,int> manager, string authenticationType)
        {
            return await manager.CreateIdentityAsync(this, authenticationType);  
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Foundation;

namespace App.Core.Entities.Identity
{
    /// <summary>
    /// 菜单
    /// </summary>
    public class MenuEntity:BaseEntity
    {
        /// <summary>
        /// 菜单标题
        /// </summary>
        public string Title { get; set; }

        public string URL { get; set; }

        /// <summary>
        /// 菜单图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 上级菜单的ID
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// 查看权限（如：Mudole1=功能1,功能2;Module2=功能1,功能2）
        /// </summary>
        public string View { get; set; }

        public int SortNo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Foundation
{
    /// <summary>
    /// 附件类
    /// </summary>
    public class AttachfileEntity:BaseEntity
    {
        /// <summary>
        /// 文件显示名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件保存路径（完整的物理路径）
        /// </summary>
        public string FileSavedPath { get; set; }

        /// <summary>
        /// 文件大小（KB）
        /// </summary>
        public string FileSize { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Foundation
{
    /// <summary>
    /// 历史记录
    /// </summary>
    public class HistoryEntity:BaseEntity
    {
        /// <summary>
        /// 操作：{Create,Modify,Approve...}
        /// </summary>
        public string Action { get; set; }
        /// <summary>
        /// 状态：{待处理，处理中，处理完成，审批完成...}
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public string Remark { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Entities.Foundation
{
    /// <summary>
    /// 基类
    /// </summary>
    public abstract class BaseEntity:IEntity<Int32>
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatedBy { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdatedDate { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public string UpdatedBy { get; set; }
    }
}

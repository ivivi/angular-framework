﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Foundation;

namespace App.Core.Entities.Vendor
{
    public class ConsultantEntity:BaseEntity
    {
        public ConsultantEntity() {
            this.Attachfiles = new List<ConsultantAttachfileEntity>();
        }

        /// <summary>
        /// 顾问姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 城市/地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string Tel { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 公司编号
        /// </summary>
        public int? VendorId { get; set; }

        /// <summary>
        /// 顾问简历 和 其他信息
        /// </summary>
        public virtual ICollection<ConsultantAttachfileEntity> Attachfiles { get; set; }

        /// <summary>
        /// 公司信息
        /// </summary>
        public virtual VendorEntity Vendor { get; set; }
    }
}

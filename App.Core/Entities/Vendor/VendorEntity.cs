﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Foundation;

namespace App.Core.Entities.Vendor
{
    public class VendorEntity:BaseEntity
    {
        public VendorEntity()
        {
            this.Attachfiles = new List<VendorAttachfileEntity>();
        }
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 公司地址
        /// </summary>
        public string CompanyAddress { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactPersion { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Tel { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        public string PostCode { get; set; }

        /// <summary>
        /// 电子邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 公司缩写
        /// </summary>
        public string Combination { get; set; } 

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 供应商营业执照 和 其他附件信息
        /// </summary>
        public virtual ICollection<VendorAttachfileEntity> Attachfiles { get; set; }

        /// <summary>
        /// 顾问信息
        /// </summary>
        public virtual ICollection<ConsultantEntity> Consultants { get; set; }
    }
}

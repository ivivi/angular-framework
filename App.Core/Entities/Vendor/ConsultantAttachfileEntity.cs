﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Foundation;

namespace App.Core.Entities.Vendor
{
    public class ConsultantAttachfileEntity:AttachfileEntity
    {
        /// <summary>
        /// 顾问Id
        /// </summary>
        public int? ConsultantId { get; set; }
        /// <summary>
        /// 顾问信息
        /// </summary>
        public virtual ConsultantEntity Consultant { get; set; }
    }
}

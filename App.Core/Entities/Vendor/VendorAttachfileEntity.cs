﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Foundation;

namespace App.Core.Entities.Vendor
{
    /// <summary>
    /// 供应商附件信息
    /// </summary>
    [Serializable]
    public class VendorAttachfileEntity:AttachfileEntity
    {
        /// <summary>
        /// 供应商Id
        /// </summary>
        public int? VendorId { get; set; }
        /// <summary>
        /// 供应商信息
        /// </summary>
        public virtual VendorEntity Vendor { get; set; }
    }
}

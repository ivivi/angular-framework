﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Foundation;

namespace App.Core.Entities.Dev
{

    public class BugAttachfileEntity: AttachfileEntity
    {
        /// <summary>
        /// Bug编号
        /// </summary>
        public int? BugId { get; set; }

        /// <summary>
        /// Bug信息
        /// </summary>
        public virtual BugEntity Bug { get; set; }
    }
}

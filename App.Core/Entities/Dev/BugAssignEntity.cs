﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Foundation;
namespace App.Core.Entities.Dev
{
    /// <summary>
    /// Bug指派记录
    /// </summary>
    [Serializable]
    public class BugAssignEntity:BaseEntity
    {
        /// <summary>
        /// 指派给X
        /// </summary>
        public string AssignTo { get; set; }

        /// <summary>
        /// Bug编号
        /// </summary>
        public int? BugId { get; set; }

        /// <summary>
        /// Bug信息
        /// </summary>
        public virtual BugEntity Bug { get; set; }
    }
}

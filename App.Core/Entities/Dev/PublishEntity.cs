﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Foundation;

namespace App.Core.Entities.Dev
{
    /// <summary>
    /// 系统发布信息
    /// </summary>
    [Serializable]
    public class PublishEntity: BaseEntity
    {
        /// <summary>
        /// 源代码版本信息
        /// </summary>
        public string SourceCodeKey { get; set; }
         
        /// <summary>
        /// 发布说明
        /// </summary>
        public string Description { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Entities.Foundation;

namespace App.Core.Entities.Dev
{
    /// <summary>
    /// Bug，以及修复情况
    /// </summary>
    [Serializable]
    public class BugEntity:BaseEntity
    {
        public BugEntity()
        {
            this.Attachfiles = new List<BugAttachfileEntity>();
            this.Histories = new List<BugHistoryEntity>();
        }

        /// <summary>
        /// Bug说明
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Bug当前处理人
        /// </summary>
        public string Assigner { get; set; }

        /// <summary>
        /// 处理状态{处理完成，不需要处理，待处理...}
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SourtNo { get; set; }

        /// <summary>
        /// 是否加急处理
        /// </summary>
        public bool IsHighline { get; set; }

        /// <summary>
        /// Bug附件信息
        /// </summary>
        public virtual ICollection<BugAttachfileEntity> Attachfiles { get; set; }
        /// <summary>
        /// 历史记录
        /// </summary>
        public virtual ICollection<BugHistoryEntity> Histories { get; set; }

        /// <summary>
        /// Bug指派信息
        /// </summary>
        public virtual ICollection<BugAssignEntity> BugAssignEntities { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Infrastructure.Extensions
{
    public static class TypeFormartExtension
    {
        /// <summary>
        /// 将一个整数表示成数值类型的字符串 如：13345456转换后：13，345，456
        /// </summary>
        /// <param name="intVal"></param>
        /// <returns></returns>
        public static string AsNumericStr(this int intVal)
        {
            var intStr = intVal.ToString();
            if (intStr.Length < 4)
                return intStr;
            var strStack = new Stack<string>();
            while (intStr.Length > 0)
            {
                if (intStr.Length > 3)
                {
                    var str = intStr.Substring(intStr.Length - 3);
                    intStr = intStr.Substring(0, intStr.Length - 3);
                    strStack.Push(str);
                }
                else
                {
                    strStack.Push(intStr);
                    intStr = String.Empty;
                }
            }
            return String.Join(",", strStack);
        }
    }
}

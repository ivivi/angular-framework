﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;

namespace App.Core.Infrastructure
{
    public class Config
    {
        public static string GetAppSettings(string key)
        {
            string value = ConfigurationManager.AppSettings[key];
            return value;
        }

        /// <summary>
        /// 获取文件上传的路径
        /// </summary>
        /// <returns></returns>
        public static string GetUploadFilePath()
        {
            string uploadFilePath = GetAppSettings("UploadFilePath");
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day;
            uploadFilePath = uploadFilePath.Replace("{Year}", year.ToString()).Replace("{Month}", month.ToString()).Replace("{Day}", day.ToString());
            if (!Directory.Exists(uploadFilePath))
                Directory.CreateDirectory(uploadFilePath);
            return uploadFilePath;
        }
    }
}
